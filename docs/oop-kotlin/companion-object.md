---
sidebar_position: 12
---

# Companion Object

An object declaration inside a class can be marked with the `companion` keyword:

:::info Note
A companion object is initialized when the corresponding class is loaded (resolved) that matches the semantics of a Java static initializer.
:::

```kotlin
class MyClass {
  companion object Factory {
    fun create(): MyClass = MyClass()
  }
}
```
Members of the companion object can be called simply by using the class name as the qualifier:

```kotlin
val instance = MyClass.create()
```
The name of the companion object can be omitted, in which case the name `Companion` will be used:

```kotlin
class MyClass {
  companion object { }
}

val x = MyClass.Companion
```
Class members can access the private members of the corresponding companion object.

The name of a class used by itself (not as a qualifier to another name) acts as a reference to the companion object of the class (whether named or not):

```kotlin
class MyClass1 {
  companion object Named { }
}

val x = MyClass1

class MyClass2 {
  companion object { }
}

val y = MyClass2
```
Note that even though the members of companion objects look like static members in other languages, at runtime those are still instance members of real objects, and can, for example, implement interfaces:

```kotlin
interface Factory<T> {
  fun create(): T
}

class MyClass {
  companion object : Factory<MyClass> {
    override fun create(): MyClass = MyClass()
  }
}

val f: Factory<MyClass> = MyClass
```
However, on the JVM you can have members of companion objects generated as real static methods and fields if you use the @JvmStatic annotation. See the [Java interoperability](https://kotlinlang.org/docs/java-to-kotlin-interop.html#static-fields) section for more detail.

## Additional Learning Materials
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/home.html)

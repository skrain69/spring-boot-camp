---
sidebar_position: 8
---

# Abstract Class, Property and Method
An abstract class is a class that cannot be instantiated. We create abstract classes to provide a common template for other classes to extend and use. You can declare an abstract class using the `abstract` keyword. 

```kotlin
abstract class Vehicle
```

An abstract class may contain both abstract and non-abstract properties and functions. You need to explicitly use the `abstract` keyword to declare a property or function as **abstract**.

```kotlin
// Concrete (Non Abstract) Properties
abstract class Vehicle(val name: String,
                       val color: String,
                       val weight: Double) {

  abstract var maxSpeed: Double

  abstract fun start()
  abstract fun stop()

  fun displayDetails() {
    println("Name: $name, Color: $color, Weight: $weight, Max Speed: $maxSpeed")
  }
}
```
Any subclass that **extends** the abstract class must implement all of its abstract methods and properties, or the subclass should also be declared as abstract. You don't need to annotate a class as `open` to allow other classes to inherit from it. Abstract classes are open for extension by default. Similarly, abstract methods and properties are open for overriding by default. But if you need to **override** a non-abstract method or property, then you must mark it with the `open` modifier.

## Extend from an Abstract class
Two concrete classes that extend the `Vehicle` abstract class and override its abstract methods and properties.

```kotlin
class Car(name: String,
          color: String,
          weight: Double,
          override var maxSpeed: Double): Vehicle(name, color, weight) {

  override fun start() {
    // function to start a car
    println("Vehicle Started")
  }

  override fun stop() {
    // function to stop a car
    println("Vehicle Stopped")
  }
}
```
main()
```kotlin
fun main() {
  val car = Car("Ferrari 812 Superfast", "red", 1525.0, 339.60)
  val motor = Car("Ducati 1098s", "red", 173.0, 271.0)

  car.displayDetails()
  motor.displayDetails()

  car.start()
  motor.start()
}

// Name: Ferrari 812 Superfast, Color: red, Weight: 1525.0, Max Speed: 339.6
// Name: Ducati 1098s, Color: red, Weight: 173.0, Max Speed: 271.0
// Vehicle Started
// Vehicle Started
```

Abstract classes help you abstract out common functionality into a base class. It may contain both abstract and non-abstract properties and methods. An abstract class is useless on its own because you cannot create objects from it. But, other concrete (non-abstract) classes can extend it and build upon it to provide the desired functionality.

## Additional Learning Materials
🔗 [Callicoder](https://www.callicoder.com/kotlin-abstract-classes/)
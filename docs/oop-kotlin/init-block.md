---
sidebar_position: 2
---

# Init Block
The code inside the init block is the first to be executed when the class is instantiated. The init block is run every time the class is instantiated.

```kotlin
class User {
  init {
    print("Class instance is initialised.")
  }

  var loggedIn: Boolean = false
  val cantChangeValue = "Hi"

  fun logOn() {
    loggedIn = true
  }

  fun logOff() {
    loggedIn = false
  }
}
```

Multiple initializer blocks can be written in a class. They’ll be executed sequentially.

```kotlin
class MultiInit(name: String) {
  init {
    println("First initializer block that prints ${name}")
  }

  init {
    println("Second initializer block that prints ${name.length}")
  }
}

fun main(args: Array<String>) {
  var multiInit = MultiInit("Kotlin")
}

// First initializer block that prints Kotlin
// Second initializer block that prints 6
```

## Additional Learning Materials
🔗 [Journal Dev](https://www.journaldev.com/18491/kotlin-class-constructor)
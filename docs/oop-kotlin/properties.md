---
sidebar_position: 4
---

# Properties
Every property declaration begins with the keyword `var` or `val`. 

## Declaring properties
Properties can be declared either as mutable or immutable. To use a property, simply refer to it by its name.

```kotlin
class Address {
  var name: String = "Holmes, Sherlock"
  var street: String = "Baker"
  var city: String = "London"
  var state: String? = null
  var zip: String = "123456"
}

fun copyAddress(address: Address): Address {
  val result = Address() // there's no 'new' keyword in Kotlin
  result.name = address.name // accessors are called
  result.street = address.street
  // ...
  return result
}
```

## Late-initialized properties 
You can mark the property with the `lateinit` modifier. This modifier can be used on `var` properties declared inside the body of a class (not in the primary constructor, and only when the property does not have a custom getter or setter). The type of the property or variable must be **non-null**, and it must not be a primitive type.

```kotlin
public class MyTest {
  lateinit var subject: TestSubject

  @SetUp fun setup() {
    subject = TestSubject()
  }

  @Test fun test() {
    // dereference directly
    subject.method()  
  }
}
```

:::note
Accessing a `lateinit` property before it has been initialized throws a special exception.
:::

### Checking whether a lateinit var is initialized
To check whether a `lateinit var` has already been initialized, use `.isInitialized` on the reference to that property.

```kotlin
if (foo::bar.isInitialized) {
  println(foo.bar)
}
```

## Overriding Properties
The overriding mechanism works on properties in the same way that it does on methods. Properties declared on a **superclass** that are then redeclared on a derived class must be prefaced with override, and they must have a compatible type. Each declared property can be overridden by a property with an initializer or by a property with a `get` method.

```kotlin
open class Shape {
  open val vertexCount: Int = 0
}

class Rectangle : Shape() {
  override val vertexCount = 4
}
```

:::tip
You can override a `val` property with a `var` property, but not vice versa.
:::

This is allowed because a val property essentially declares a `get` method, and overriding it as a `var` additionally declares a `set` method in the derived class.

:::note
You can use the `override` keyword as part of the property declaration in a primary constructor.
:::

```kotlin
interface Shape {
  val vertexCount: Int
}

class Rectangle(override val vertexCount: Int = 4) : Shape // Always has 4 vertices

class Polygon : Shape {
  override var vertexCount: Int = 0  // Can be set to any number later
}
```

## Additional Learning Materials
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/properties.html)
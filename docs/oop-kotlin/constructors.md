---
sidebar_position: 3
---

# Primary and Secondary Constructors
Constructors are special member functions that are used to initialize properties. There are primary and secondary constructors. The primary constructor initializes the class, while the secondary constructor is used to initialize the class and introduce some extra logic.

## Primary Constructors
The primary constructor is initialized in the class header, and goes after the class name, using the `constructor` keyword. The parameters are optional.

```kotlin
class Person constructor(firstName: String) { /*...*/ }
```
If the primary constructor does not have any annotations or visibility modifiers, the `constructor` keyword can be **omitted**. The **primary** constructor cannot contain any code. Initialization code can be placed in initializer blocks prefixed with the `init` keyword. During the initialization of an instance, the initializer blocks are executed in the same order as they appear in the class body.

```kotlin
class InitOrderDemo(name: String) {
  val firstProperty = "First property: $name".also(::println)
    
  init {
    println("First initializer block that prints ${name}")
  }
    
  val secondProperty = "Second property: ${name.length}".also(::println)
    
  init {
    println("Second initializer block that prints ${name.length}")
  }
}
```
Its parameters can be used in the **initializer blocks**. They can also be used in property initializers declared in the class body.

```kotlin
class Customer(name: String) {
  val customerKey = name.uppercase()
}
```

You can declare properties and initialize them from the primary constructor. Such declarations can also include default values of the class properties.

```kotlin
class Person(val firstName: String, val lastName: String, var isEmployed: Boolean = true)
```

## Secondary Constructors
Secondary constructors allow initialization of variables and logic to the class as well. They are prefixed with the `constructor` keyword. Delegation to another constructor of the same class is done using the `this` keyword.

```kotlin
class Person(val name: String) {
  var children: MutableList<Person> = mutableListOf()
  constructor(name: String, parent: Person) : this(name) {
    parent.children.add(this)
  }
}
```
Delegation to the primary constructor happens as the **first** statement of a secondary constructor. The code in all initializer blocks and property initializers is executed before the body of the secondary constructor. Even if the class has **no primary constructor**, the delegation still happens implicitly, and the initializer blocks are still executed.

```kotlin
class Constructors {
  init {
    println("Init block")
  }

  constructor(i: Int) {
    println("Constructor $i")
  }
}

// Init block
// Constructor 1
```

If a **non-abstract** class does not declare any constructors, it will have a generated primary constructor with no arguments. The visibility of the constructor will be **public**. If you don't want your class to have a public constructor, declare an empty primary constructor with non-default visibility.

```kotlin
class DontCreateMe private constructor () { /*...*/ }
```

## Additional Learning Materials
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/classes.html#constructors)
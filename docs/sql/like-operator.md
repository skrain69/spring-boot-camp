---
sidebar_position: 8
---

# LIKE Operator

The `LIKE` operator is used in a `WHERE` clause to search for a specified pattern in a column.

There are two [wildcards](https://google.com "Wildcards") often used in conjunction with the `LIKE` operator:

👉 The percent sign (%) represents zero, one, or multiple characters <br/>
👉 The underscore sign (_) represents one, single character

:::info Note
MS Access uses an asterisk (*) instead of the percent sign (%), and a question mark (?) instead of the underscore (_).
:::

:::tip Tip
The percent sign and the underscore can also be used in combinations! <br/>
You can also combine any number of conditions using `AND` or `OR` operators.
:::

## Basic Syntax

```sql
SELECT column1, column2, ...
FROM table_name
WHERE columnN LIKE pattern;
```

Here're some examples that show how to use the LIKE operator with '%' and '_' wildcards:

| Statement	             | Meaning	                        | Values Returned  |                                                                                    
|------------------------|----------------------------------|------------------|
| WHERE name LIKE 'Da%'  |	Find names beginning with 'Da'  |	Dario, Dartmouth
| WHERE name LIKE '%th'  |	Find names ending with 'th'	  | Margareth, Smith
| WHERE name LIKE '%on%' |	Find names containing the 'on'  |	Wilkinson, Tony
| WHERE name LIKE 'Sa_'	 | Find names beginning with 'Sa' and is followed by at most one character |	Sal
| WHERE name LIKE '_ay'	 | Find names ending with 'ay' and is preceded by at most one character |	Jay, May
| WHERE name LIKE '_an_' |	Find names containing 'an' and begins and ends with at most one character |	Jana, Vans
| WHERE name LIKE '%ar_' |	Find names containing 'ar', begins with any number of characters, and ends with at most one character |	Richard, Larl
| WHERE name LIKE '_ar%' |	Find names containing 'ar', begins with at most one character, and ends with any number of characters |	Darl, Darida

## DEMO Database 

Below is an example table named "Employees":

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |    NULL |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

### LIKE Example

The following SQL statement finds all employees whose names begin with the letter 'S' from the "Employees" table:

```sql
SELECT * FROM Employees 
WHERE emp_name LIKE 'S%';
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |



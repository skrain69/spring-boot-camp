---
sidebar_position: 7
---

# INSERT INTO Statement

The `INSERT INTO` statement is used to insert new rows in a database table.

## Basic Syntax

### Syntax I

Specify both the column names and the values to be inserted.

```sql
INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);
```
### Syntax II

There is no need to specify the column names in the SQL query if you are adding values for all the columns of the table as long as the order of the values is in the same order as the columns in the table.

```sql
INSERT INTO table_name
VALUES (value1, value2, value3, ...);
```
## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |

### INSERT INTO Example

The following SQL statement inserts a new record in the "Restaurants" table:

```sql
INSERT INTO Restaurants ( RestaurantName, Address, City, Country)
VALUES ('Crisostomo', '2nd Floor, The Newport Mall, Newport Boulevard', 'Pasay', 'Philippines');
```

After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  Philippines       |

:::info Note
The RestaurantID column is an auto-increment field and will be generated automatically when a new record is inserted into the table.
:::

### Insert Data Only in Specified Columns

It is also possible to only insert data in specific columns.

The following SQL statement will insert a new record, but only insert data in the "RestaurantName",  and "City" columns:

```sql
INSERT INTO Restaurants (RestaurantName, City)
VALUES ('Kai', 'Makati');
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  Philippines       |
| 7              |      Kai               |                 null                              |  Makati    |       null         |


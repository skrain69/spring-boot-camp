---
sidebar_position: 24
---

# CAST() Function

The `CAST()` function converts a value (of any type) into a specified datatype.

## Basic Syntax

```sql
CAST(expression AS datatype(length))
```
## Parameter Values

|Value |	Description|
|------|---------------|
|expression	| Required. The value to convert |
|datatype	|Required. The datatype to convert expression to. Can be one of the following: bigint, int, smallint, tinyint, bit, decimal, numeric, money, smallmoney, float, real, datetime, smalldatetime, char, varchar, text, nchar, nvarchar, ntext, binary, varbinary, or image|
|(length)	|Optional. The length of the resulting data type (for char, varchar, nchar, nvarchar, binary and varbinary)|

:::info Note
CAST() works in SQL Server (starting with 2008), Azure SQL Database, Azure SQL Data Warehouse, and Parallel Data Warehouse.
:::

## CAST() Examples

### Converting a value to a varchar datatype

The following statement uses the CAST() function to convert the decimal number 19.19 to a set of character data of indeterminate length:

```sql
SELECT CAST(19.19 AS varchar);
```
After executing the above statement, you'll get this output:

| 19.19                   | 
|-------------------------|

### Converting a value to a datetime datatype

The following statement uses the CAST() function to convert the string '2021-09-25' to a datetime:

```sql
SELECT CAST('2021-09-25' AS datetime);
```
After executing the above statement, you'll get this output:

| 2021-09-25 00:00:00.000 | 
|-------------------------|

### Converting a decimal to an integer datatype

The following statement uses the CAST() function to convert the decimal number 5.95 to an integer:

```sql
SELECT CAST(5.95 AS INT) result;
```
After executing the above statement, you'll get this output:

| result                  | 
|-------------------------|
|   5                     | 

### Converting a decimal to another decimal with different length

The following statement uses the CAST() function to convert the decimal number 5.95 to another decimal number with the zero scale:

```sql
SELECT CAST(5.95 AS DEC(3,0)) result;
```
After executing the above statement, you'll get this output:

| result                  | 
|-------------------------|
|   6                     | 

When you convert a value of the data types in different places, SQL Server will return a truncated result or a rounded value based on the following rules:

|From Data Type |	To Data Type	| Behavior | 
|---------------|-------------------|----------|
|numeric	    |numeric	        |  Round   |
|numeric	    |int	            |Truncate  |
|numeric	    |   money           |	Round  |
|money          |	int	            |  Round   |
|money	        |   numeric         | 	Round  |
|float          |	int	            |Truncate  |
|float	        |  numeric	        |Round     |
|float	        | datetime	        | Round    |
|datetime	    |   int	            | Round    |
 
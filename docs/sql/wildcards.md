---
sidebar_position: 22
---

# Wildcards

A wildcard character is used to substitute one or more characters in a string. 

:::info Note
Wildcard characters are used with the `LIKE` operator. The `LIKE` operator is used in a `WHERE` clause to search for a specified pattern in a column.
:::

## Wildcard Characters 

### MS Access

| Symbol  | 	Description                                                                         |         	Example                     |
|---------|-----------------------------------------------------------------------------------------|---------------------------------------|
|*	      | Represents zero or more characters                                                      |	bl* finds bl, black, blue, and blob |
|?	      | Represents a single character                                                           |	h?t finds hot, hat, and hit         |
|[ ]	  | Represents any single character within the brackets	                                    | h[oa]t finds hot and hat, but not hit |
|!	      | Represents any character not in the brackets	                                        | h[!oa]t finds hit, but not hot and hat|
|-	      | Represents any single character within the specified range                              |	c[a-b]t finds cat and cbt           |
|#	      | Represents any single numeric character	                      | 2#5 finds 205, 215, 225, 235, 245, 255, 265, 275, 285, and 295  |

### SQL Server

| Symbol  | 	Description                                                                         |         	Example                     |
|---------|-----------------------------------------------------------------------------------------|---------------------------------------|
|%	      | Represents zero or more characters                                                      |	bl% finds bl, black, blue, and blob |
|_	      | Represents a single character                                                           |	h_t finds hot, hat, and hit         |
|[ ]	  | Represents any single character within the brackets	                                    |h[oa]t finds hot and hat, but not hit  |
|^	      | Represents any character not in the brackets	                                        | h[^oa]t finds hit, but not hot and hat|
|-	      | Represents any single character within the specified range                              |	c[a-b]t finds cat and cbt           |

:::info Note
All the wildcards can also be used in combinations!
:::

Here are some examples that show how to use the `LIKE` operator with '%' and '_' wildcards:

| Statement	             | Meaning	                        |                                                                                   
|------------------------|----------------------------------|
| WHERE name LIKE 'Da%'  |	Find names beginning with 'Da'  |	
| WHERE name LIKE '%th'  |	Find names ending with 'th'	    | 
| WHERE name LIKE '%on%' |	Find names containing the 'on'  |	
| WHERE name LIKE 'Sa_'	 | Find names beginning with 'Sa' and is followed by at most one character |	
| WHERE name LIKE '_ay'	 | Find names ending with 'ay' and is preceded by at most one character |	
| WHERE name LIKE '_an_' |	Find names containing 'an' and begins and ends with at most one character |	
| WHERE name LIKE '%ar_' |	Find names containing 'ar', begins with any number of characters, and ends with at most one character |
| WHERE name LIKE '_ar%' |	Find names containing 'ar', begins with at most one character, and ends with any number of characters |	

## DEMO Database 

Below is an example table named "Employees":

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |    NULL |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

### Using %

The following SQL statement finds all employees whose names begin with the letter 'J' from the "Employees" table:

```sql
SELECT * FROM Employees 
WHERE emp_name LIKE 'J%';
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |

### Using _

The following SQL statement finds all employees whose names begin with any character, followed by "orty":

```sql
SELECT * FROM Employees
WHERE emp_name LIKE '_orty';
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |

### Using [ ]

#### Example I

The following SQL statement finds all employees whose names begin with "j", "h", or "s":

```sql
SELECT * FROM Employees
WHERE emp_name LIKE '[jhs]%';
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

#### Example II

The following SQL statement finds all employees whose names begin with "r", or "s":

```sql
SELECT * FROM Employees
WHERE emp_name LIKE '[r-s]%';
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

#### Example III

The following SQL statements select all employees whose names do NOT begin with "j", "h" or "s":

```sql
SELECT * FROM Employees
WHERE emp_name LIKE '[!jhs]%';
```
or

```sql
SELECT * FROM Employees
WHERE emp_name NOT LIKE '[jhs]%';
```

After executing the above statements, you'll get the same output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |    NULL |






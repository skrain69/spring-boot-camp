---
sidebar_position: 3
---

# WHERE Clause

The `WHERE` clause is used with the `SELECT` statement to filter records or extract only those records that fulfill specified conditions. 

:::tip 
The `WHERE` clause is not only used in `SELECT` statements, it is also used in `UPDATE`, `DELETE`, etc.!
:::

## Basic Syntax

```sql
SELECT column1_name, column2_name, ...
FROM table_name
WHERE condition;
```
## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |

### WHERE Clause Example

The following SQL statement selects all the restaurants from the city "Makati", in the "Restaurants" table:

```sql
SELECT * FROM Restaurants
WHERE City='Makati';
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |

As you can see the output contains only those restaurants located in "Makati" city. Similarly, you can fetch records from specific columns, like this:


### Text Fields vs. Numeric Fields

SQL requires single quotes around text values (most database systems will also allow double quotes).
However, numeric fields should not be enclosed in quotes:

```sql
SELECT * FROM Restaurants
WHERE RestaurantID=1;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |

## Operators Allowed in WHERE Clause

SQL supports a number of different operators that can be used in `WHERE` clause, the most important ones are summarized in the following table.


|   Operator     |          Description          |        Example        |
|----------------|:-----------------------------:|:---------------------:|
|    =           |    Equal                      |    WHERE id = 8       |
|    >           |    Greater than               |    WHERE num > 2      |
|    <           |    Less than                  |    WHERE num < 3      |
|    >=          |    Greater than or equal      |    WHERE score >= 80  |
|    <=          |    Less than or equal         |    WHERE age <= 32    |
|    LIKE        |    Simple pattern matching    |    WHERE name LIKE 'Lee' |
|    IN          |    Check whether a specified value matches any value in a list or subquery  |    WHERE city IN ('Makati','Pasay')   |                 
|    BETWEEN     |    Check whether a specified value is within a range of values  | WHERE rating BETWEEN 75 AND 90 |
                      
                      
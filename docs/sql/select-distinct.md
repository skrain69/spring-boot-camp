---
sidebar_position: 2
---

# SELECT Distinct

The `SELECT DISTINCT` statement is used to return only distinct (different) values.<br/>
Inside a table, a column often contains many duplicate values; and sometimes you only want to list the different (distinct) values.

## Retrieving Distinct Values

When fetching data from a database table, the result set may contain duplicate rows or values. <br/>
If you want to remove these duplicate values you can specify the keyword `DISTINCT` directly after the `SELECT` keyword, as demonstrated below:

## Basic Syntax

The `DISTINCT` clause is used to remove duplicate rows from the result set:

```sql
SELECT DISTINCT column1_name, column2_name, ...
FROM table_name;
```
:::info Note 
The `DISTINCT` clause behaves similar to the **UNIQUE** constraint, except in the way it treats nulls. 
Two **NULL** values are considered unique, while at the same time they are not considered distinct from each other.
:::

## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |

### SELECT Example Without DISTINCT

The following SQL statement selects all (including the duplicates) values from the "City" column in the "Restaurants" table:

```sql
SELECT City FROM Restaurants;
```
After executing the above statement, you'll get this output:

|    City    |
|:----------:|
|   Taguig   |  
|   Makati   |
|   Pasig    |
|   Makati   |
|   Taguig   |  

As you can see, you'll find the cities **Makati** and **Taguig** appear twice in our result set. This can be fixed by using `DISTINCT`.

### SELECT Example with DISTINCT

The following SQL statement selects only the DISTINCT values from the "City" column in the "Restaurants" table:

```sql
SELECT DISTINCT City FROM Restaurants;
```
After executing the above statement, you'll get this output:

|    City    |
|:----------:|
|   Taguig   |  
|   Makati   |
|   Pasig    |
 
As you see this time there is no duplicate values in our result set.

:::info Note 
If you use the `SELECT DISTINCT` statement for a column that has multiple **NULL** values, Then SQL keeps one **NULL** value and removes others from the result set, because `DISTINCT` treats all the **NULL** values as the same value.
:::





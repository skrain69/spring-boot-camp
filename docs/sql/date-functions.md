---
sidebar_position: 23
---

# Date Functions

## SQL Date Data Types

### MySQL comes with the following data types for storing a date or a date/time value in the database:

📌 **DATE** - format YYYY-MM-DD <br/>
📌 **DATETIME** - format: YYYY-MM-DD HH:MI:SS <br/>
📌 **TIMESTAMP** - format: YYYY-MM-DD HH:MI:SS <br/>
📌 **YEAR** - format YYYY or YY

### SQL Server comes with the following data types for storing a date or a date/time value in the database:

📌 **DATE** - format YYYY-MM-DD <br/>
📌 **DATETIME** - format: YYYY-MM-DD HH:MI:SS <br/>
📌 **SMALLDATETIME** - format: YYYY-MM-DD HH:MI:SS <br/>
📌 **TIMESTAMP** - format: a unique number

:::info Note
The date types are chosen for a column when you create a new table in your database!
:::

## DEMO Database

Below is an example table named "Orders":

|OrderId|	ProductName	|OrderDate|
|-------|---------------|---------|
|1|	Geitost|	2008-11-11|
|2|	Camembert Pierrot|	2008-11-09|
|3|	Mozzarella di Giovanni|	2008-11-11|
|4|	Mascarpone Fabioli|	2008-10-29|   

### Example 

Now we want to select the records with an OrderDate of "2008-11-11" from the "Orders" table. 

```sql
SELECT * FROM Orders WHERE OrderDate='2008-11-11';
```

After executing the above statement, you'll get this output:

|OrderId|	ProductName	|OrderDate|
|-------|---------------|---------|
|1|	Geitost|	2008-11-11|
|3|	Mozzarella di Giovanni|	2008-11-11|

:::info Note
Two dates can easily be compared if there is no time component involved!
:::

Now, assume that the "Orders" table looks like this (notice the added time-component in the "OrderDate" column):

|OrderId|	ProductName	|OrderDate|
|-------|---------------|---------|
|1|	Geitost|	2008-11-11  13:23:44|
|2|	Camembert Pierrot|	2008-11-09 15:45:21|
|3|	Mozzarella di Giovanni|	2008-11-11 11:12:01|
|4|	Mascarpone Fabioli|	2008-10-29 14:56:59|   

If we use the same `SELECT` statement:

```sql
SELECT * FROM Orders WHERE OrderDate='2008-11-11';
```

We will get no result! This is because the query is looking only for dates with no time portion.

:::tip 
To keep your queries simple and easy to maintain, do not use time-components in your dates, unless you have to!
:::



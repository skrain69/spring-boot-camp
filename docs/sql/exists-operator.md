---
sidebar_position: 20
---

# Exists Operator

The `EXISTS` operator is used to test for the existence of any record in a subquery. It also returns `TRUE` if the subquery returns one or more records.

## Basic Syntax

```sql
SELECT column_name
FROM table_name
WHERE EXISTS
(SELECT column_name FROM table_name WHERE condition);
```

## DEMO Database 

Below are two example tables named "Products" and "Suppliers":

#### Products

| ProductID   |    ProductName     |      SupplierID  | CategoryID   |    Unit       |         Price        |
|-------------|:------------------:|:----------------:|:------------:|:-------------:|:--------------------:|
| 1           |  Sweet Corn        |          1       |     1        |  100 kgs      |    9000        |
| 2           |    Pork Loin       |  2              |         2           |  80 kgs        |   18080        |
| 3           |   Soda             |    3             |        3      | 30 boxes x 24 350ml cans    |   18720   |
| 4           |  Cajun sauce       |    4             |      4        | 15 boxes x  24 250ml bottles  |    54000     |
| 5           |   Beef short ribs  |   2              |     2      |  80 kgs       |     32000           |

#### Suppliers

| SupplierID  |    SupplierName    |     ContactName  |        Address        |    City      |       
|-------------|:------------------:|:----------------:|:---------------------:|:------------:|
| 1           |  King Corn Trading |       John Doe   |    Caruncho Ave       |     Pasig    |  
| 2           |   Kitayama Meat    |  Jane Dow        |        Chino Roces    |  Makati      |  
| 3           |   Coca Cola        |    Jack Dowe     |      McArthur Hwy     | San Fernando |  
| 4           |  Pines Traders     |   Jill Duo       |     Naga Road         | Las Piñas    |    

### Exists Examples

#### Example I
The following SQL statement returns `TRUE` and lists the suppliers with a product price less than 10000:

```sql
SELECT SupplierName
FROM Suppliers
WHERE EXISTS (SELECT ProductName FROM Products WHERE Products.SupplierID = Suppliers.supplierID AND Price < 10000);
```
After executing the above statement, you'll get this output:

| SupplierID  |    SupplierName    |     ContactName  |        Address        |    City      |       
|-------------|:------------------:|:----------------:|:---------------------:|:------------:|
| 1           |  King Corn Trading |       John Doe   |    Caruncho Ave       |     Pasig    |  

#### Example II
The following SQL statement returns `TRUE` and lists the suppliers with a product price equal to 18080:

```sql
SELECT SupplierName
FROM Suppliers
WHERE EXISTS (SELECT ProductName FROM Products WHERE Products.SupplierID = Suppliers.supplierID AND Price = 18080);
```
After executing the above statement, you'll get this output:

| SupplierID  |    SupplierName    |     ContactName  |        Address        |    City      |       
|-------------|:------------------:|:----------------:|:---------------------:|:------------:|
| 2           |   Kitayama Meat    |  Jane Dow        |        Chino Roces    |  Makati      | 


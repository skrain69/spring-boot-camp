---
sidebar_position: 10
---

# BETWEEN and NOT BETWEEN Operators

Sometimes you want to select a row if the value in a column falls within a certain range. This type of condition is common when working with numeric data. 
To perform the query based on such condition you can utilize the `BETWEEN` operator. 

👉 The `BETWEEN` operator selects values within a given range. The values can be **numbers**, **text**, or **dates**. <br/>
👉 The `BETWEEN` operator is inclusive: begin and end values are included. 

Similarly, you can use the `NOT BETWEEN` operator, which is the exact opposite of `BETWEEN`.

## Basic Syntax

### BETWEEN Syntax

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name BETWEEN value1 AND value2;
```
### NOT BETWEEN Syntax

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name NOT BETWEEN value1 AND value2;
```

## DEMO Database 

Below is an example table named "Employees":

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |       2 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

### BETWEEN Example

The following SQL statement selects all employees with a salary between 15000 and 16000:

```sql
SELECT * FROM Employees
WHERE salary BETWEEN 15000 AND 16000;
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      1 | Johnny Cage      | 2020-05-11 |   15000 |       4 |
|      2 | Hannah Montana   | 2020-06-25 |   15500 |       1 |
|      5 | Martin Meachum   | 2018-07-14 |   15600 |       2 |
|      6 | Simon Baker      | 2019-05-21 |   16000 |       1 |

### NOT BETWEEN Example

The following SQL statement selects all employees with a salary NOT between 15000 and 16000:

```sql
SELECT * FROM Employees
WHERE salary NOT BETWEEN 15000 AND 16000;
```
After executing the above statement, you'll get this output:

| emp_id | emp_name         | hire_date  | salary  | dept_id |
|--------|------------------|------------|-------- |---------|
|      3 | Sarah Parker     | 2019-11-28 |   18000 |       5 |
|      4 | Rick Morty       | 2016-02-23 |   17200 |       3 |


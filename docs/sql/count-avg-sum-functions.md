---
sidebar_position: 15
---

# COUNT(), AVG() and SUM() Functions

The `COUNT()` function returns the number of rows that matches a specified criterion.<br/>
The `AVG()` function returns the average value of a numeric column. <br/>
The `SUM()` function returns the total sum of a numeric column. 


## Basic Syntax

### COUNT()
```sql
SELECT COUNT(column_name)
FROM table_name
WHERE condition;
```
### AVG()
```sql
SELECT AVG(column_name)
FROM table_name
WHERE condition;
```

### SUM()
```sql
SELECT SUM(column_name)
FROM table_name
WHERE condition;
```
## DEMO Database 

Below is an example table named "Groceries":

| ItemID         |     ItemName           |                  Price                            | 
|----------------|:----------------------:|:-------------------------------------------------:|
| 1              |    Liquid Hand Soap    |                115. 1                             |    
| 2              |    Soda                |                 29.5                              |        
| 3              |    Chocolate Bar       |                 67.5                              |            
| 4              |    Ice cream tub       |                313                                |           
| 5              |    Canned fruit        |                 55                                |           
| 6              |    Baby wipes          |                 89                                |           
| 7              |    Toothpaste          |                 231                               |            
| 8              |    Yogurt              |                 110                               |   

### COUNT() Example

The following SQL statement finds the number of items in "Groceries":

```sql
SELECT COUNT(ItemID)
FROM Groceries;
```
After executing the above statement, you'll get this output:

| COUNT(ItemID) |
|---------------|
| 8             |

:::info Note
NULL values are not counted and ignored.
:::

### AVG() Example

The following SQL statement finds the average price of all items in "Groceries":

```sql
SELECT AVG(Price)
FROM Groceries;
```
After executing the above statement, you'll get this output:

| AVG(Price)    |
|---------------|
|   126.2625    |

### SUM() Example

The following SQL statement finds the sum of the "Price" field in "Groceries":

```sql
SELECT SUM(Price)
FROM Groceries;
```
After executing the above statement, you'll get this output:

| SUM(Price)    |
|---------------|
|   1,010.1     |




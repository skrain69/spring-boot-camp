---
sidebar_position: 13
---

# ORDER BY Keyword

When you use the `SELECT` statement to fetch data from a table, the rows in result set are not in any particular order. If you want your result set in a particular order, you can specify the `ORDER BY` clause at the end of the statement which tells the server how to sort the data returned by the query. <br/>

👉 The `ORDER BY` keyword is used to sort the result-set in ascending or descending order.<br/>
👉 The `DESC` keyword is used to sort the records in descending order.

:::info Note 
By default, the `ORDER BY` keyword sorts the records in ascending order
:::

## Basic Syntax

```sql
SELECT column1_name, column2_name ...
FROM table_name
ORDER BY column1_name, column2_name ... ASC|DESC;
```
## DEMO Database 

Below is an example table named "Students":

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |
| 010225         | Ben White              |  London                  |  England             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |       
| 010228         |  Jürgen Koch           |  Frankfurt               | Germany              |
| 010229         |  Jürgen Wagner         |  Frankfurt               |  Germany             |
| 010230         | Debbie Fischer         |  Cambridge               | England              |

### ORDER BY Example

The following SQL statement selects all students from the "Students" table, sorted by the "Country" column:

```sql
SELECT * FROM Students
ORDER BY Country;
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010225         | Ben White              |  London                  |  England             |
| 010230         | Debbie Fischer         |  Cambridge               |  England             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010228         |  Jürgen Koch           |  Frankfurt               | Germany              |
| 010229         |  Jürgen Wagner         |  Frankfurt               |  Germany             |
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |  

### ORDER BY DESC Example
The following SQL statement selects all students from the "Students" table, sorted DESCENDING by the "Country" column:

```sql
SELECT * FROM Students
ORDER BY Country DESC;
```
After executing the above statement, you'll get this output:


|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |  
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010228         |  Jürgen Koch           |  Frankfurt               | Germany              |
| 010229         |  Jürgen Wagner         |  Frankfurt               |  Germany             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010225         | Ben White              |  London                  |  England             |
| 010230         | Debbie Fischer         |  Cambridge               |  England             |

## Sorting Multiple Columns

You can also specify multiple columns while sorting. However, changes in the result set will only be visible if you have duplicate values in your table. <br/><br/>
To understand the multi-column sorting in a better way, let's assume that we've a table named "Drivers" in our database with the following records:

| id | first_name | last_name  |  gender |
|----|------------|------------|---------|
|  1 | Peter      | Jones      |   M     |
|  2 | Harry      | Kane       |   M     |
|  3 | Peter      | James      |   M     |
|  4 | Alice      | Brewer     |   F     |
|  5 | John       | Doe        |   M     |

In the "Drivers" table, you will find "Peter" twice under the first_name column. These are duplicate values. 

:::info Note 
When multiple sort columns are specified, the result set is initially sorted by the first column and then followed by the second column, and so on.
:::

### ORDER BY Multiple Columns Example

#### Example I
```sql
SELECT * FROM Drivers
ORDER BY first_name;
```
After executing the above statement, you'll get this output:

| id | first_name | last_name  |  gender |
|----|------------|------------|---------|
|  4 | Alice      | Brewer     |   F     |
|  2 | Harry      | Kane       |   M     |
|  5 | John       | Doe        |   M     |
|  1 | Peter      | Jones      |   M     |
|  3 | Peter      | James      |   M     |

#### Example II
```sql
SELECT * FROM Drivers
ORDER BY first_name, last_name;
```
After executing the above statement, you'll get this output:

| id | first_name | last_name  |  gender |
|----|------------|------------|---------|
|  4 | Alice      | Brewer     |   F     |
|  2 | Harry      | Kane       |   M     |
|  5 | John       | Doe        |   M     |
|  3 | Peter      | James      |   M     |
|  1 | Peter      | Jones      |   M     |


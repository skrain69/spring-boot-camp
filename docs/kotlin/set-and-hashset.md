---
sidebar_position: 10
---

# Set and HashSet

 **HashSet** is a generic unordered collection of elements and it does not contain duplicate elements. It implements the **set** interface. The function `hashSetOf()` returns a mutable HashSet, which can be both **read** and **written**. The HashSet class store all the elements using hashing mechanism.

:::note
Set is the **general** interface to a set-like collection, like HashSet. HashSet is a **specific** implementation of the set interface (which uses hash codes, hence the name).
:::

It returns a new HashSet with the given elements but does not guarantee about the order sequence specified at the storing time.

```kotlin
fun main(args: Array<String>)
{ 
  // declares a hashset of integers
  val setA = hashSetOf(1,2,3,3)

  println(setA) 
  // [1, 2, 3]
      
  // declares a hashset of strings
  val setB = hashSetOf("Set","and","HashSet")

  println(setB) 
  // [Set, and, HashSet]     
}

// [1, 2, 3]
// [Set, and, HashSet]
```

## Add and remove elements in hashset
We can add elements in a hashset using `add()` and `addAll()` functions.

```kotlin
fun main(args: Array<String>)
{
  // declares a hashset of integers
  val setA = hashSetOf<Int>();

  println(setA) 
  // {}
  
  // adds elements
  setA.add(1)
  setA.add(2)
  
  // creates another set 
  val newSet = setOf(4,5,6)

  // adds newSet elements to setA
  setA.addAll(newSet)
  
  println(setA) 
  // [1, 2, 4, 5, 6]
}

// []
// [1, 2, 4, 5, 6]
```
We can remove an element using `remove()` function.

```kotlin
// removes element 2 from the set
setA.remove(2)

println(setA) 
// [1, 4, 5, 6] 
```

## Traversal in hashSet
We can traverse a hashSet using an iterator in a loop.
```kotlin
fun main(args: Array<String>)
{
  // declares a hashset of integers
  val setA = hashSetOf(1,2,3,5);
      
  // traverses in a set using a for loop and prints each item
  for(item in setA)
    println(item)
}

// 1
// 2
// 3
// 5
```

## HashSet Indexing
Using index functions `indexOf()` , `lastIndexOf()` we can get the index of the specified element. And we can also find the elements at some specific index using `elementAt()` function.

```kotlin
fun main(args: Array<String>) {
  // declares a hashset of strings
  val captains = hashSetOf("Kohli","Smith","Root","Malinga","Rohit","Dhawan")

  println("The element at index 3 is: " + captains.elementAt(3))  
  // The element at index 3 is: Smith

  println("The index of element is: " + captains.indexOf("Smith"))  
  // The index of element is: 3

  println("The last index of element is: " + captains.lastIndexOf("Rohit"))  
  // The last index of element is: 0 
}

// The element at index 3 is: Smith
// The index of element is: 3
// The last index of element is: 0
```

To check whether an element is present in the Hashset or not, we use `contains()` and `containsAll()` functions.

```kotlin
fun main(args: Array<String>){
  // declares a hashset of integers and strings
  val captains = hashSetOf(1,2,3,4,"Kohli","Smith", "Root","Malinga","Rohit","Dhawan")
 
  // stores "Rohit" to a variable 
  var name = "Rohit"

  println("The set contains the element $name or not?" + " " + captains.contains(name))
  // The set contains the element Rohit or not? true     
 
  println("The set contains the given elements or not?" + " " + captains.containsAll(setOf(1,3,"Dhawan","Warner")))
  // The set contains the given elements or not? false 
}

// The set contains the element Rohit or not? true
// The set contains the given elements or not? false
```

## Additional Learning Materials
🔗 [Geeks for Geeks](https://www.geeksforgeeks.org/kotlin-hashsetof/) 
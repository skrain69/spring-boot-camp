---
sidebar_position: 5
---

# Operators
An operator is a symbol that tells the compiler to perform specific mathematical or logical manipulations. In this guide, we will learn types of operators available in Kotlin and how to use them.

They are grouped into six:<br/>
👌 Arithmetic operators `+` `-` `/` `*` <br/>
👌 Relational operators `>` `<` `>=` `<=` `==` `!=`<br/>
👌 Assignment operators `=` `+=` `-=` `*=` `/=` `%=`<br/>
👌 Unary operators `+` `-` `++` `--` `!`<br/>
👌 Logical operators `&&` `||` `!`<br/>
👌 Bitwise operations 


### Arithmetic Operators

|    Operator    |        Name         |             Description                 |           Example           | 
|----------------|:-------------------:|:---------------------------------------:|:---------------------------:|
| +              |      Addition       |        Adds together two values         |            x + y            |   
| -              |     Subtraction     |     Subtracts one value from another    |            x - y            |
| *              |    Multiplication   |          Multiplies two values          |            x * y            |
| /              |      Division       |       Divides one value by another      |      	  x / y            |   
| %              |      Modulus        |    Returns the division remainder       |            x % y            |   

```kotlin
fun main (args: Array<String>) {
  val x:Int = 40
  val y:Int = 20
  
  // addition
  println("x + y = " + (x + y)) 
  // x + y = 60

  // subtraction
  println("x - y = " + (x - y)) 
  // x - y = 20
  
  // division
  println("x / y = " + (x / y)) 
  // x / y = 2

  // multiplication
  println("x * y = " + (x * y)) 
  // x * y = 800

  // modulus
  println("x % y = " + (x % y)) 
  // x % y = 0
}
```

:::note
The addition operator `+` is also used for the **string concatenation**. 
:::

### Relational Operators
Relational operators are used to compare two values, and returns a Boolean value-- either `true` or `false`.

|    Operator    |             Name              |             Example           | 
|----------------|:-----------------------------:|:-----------------------------:|
| >              |          greater than         |              x + y            |   
| <              |           less than           |              x - y            |
| >=             |    greater than or equal to   |              x * y            |
| <=             |      less than or equal to    |              x / y            |   
| ==             |           is equal to         |              x % y            |   
| !=             |         is not equal to       |              x % y            |   

```kotlin
fun main(args: Array<String>) {
  val x:Int = 40
  val y:Int = 20

  // greater than
  println("x > y = " +  (x > y)) 
  // x > y = true

  // less than
  println("x < y = " +  (x < y)) 
  // x < y = false
  
  // greater than or equal to
  println("x >= y = " +  (x >= y)) 
  // x >= y = true

  // less than or equal to
  println("x <= y = " +  (x <= y)) 
  // x <= y = false

  // equal to
  println("x == y = " +  (x == y)) 
  // x == y = false

  // not equal to
  println("x != y = " +  (x != y)) 
  // x != y = true
}
```

### Assignment Operators
They are used to assign values to variables.

```kotlin
fun main(args: Array<String>) {
  val x:Int = 40
  val y:Int = 20

  // variable x is assigned a value of 40 
  println("x = " +  x) 
  // x = 40

  // variable y is assigned a value of 20 
  println("y = " +  y) 
  // y = 20
}
```
:::note
When you use `+=`, it will assign value of the sum of itself and the value given.
:::

```kotlin
fun main(args: Array<String>) {
  var x:Int = 40

  // x = x + 10
  x += 10
      
  println("x = " +  x) 
  // x = 50
}
```

This works for `-=` `*=` `/=` `%=` as well.

### Unary Operators
They perform various operations such as **incrementing/decrementing** a value by one, **negating** an expression, or **inverting** the value of a boolean.

|    Operator    |             Name              |             Example           | 
|----------------|:-----------------------------:|:-----------------------------:|
| +              |          unary plus           |              +x               |   
| -              |          unary minus          |              -x               |
| ++             |         increment by 1        |              ++x              |
| --             |         decrement by 1        |              --x              |      
| !              |inverts the value of a boolean |              !x               | 

```kotlin
fun main(args: Array<String>) {
  var x:Int = 40
  var b:Boolean = true

  // sets x as positive 
  println("+x = " +  (+x)) 
  // +x = 40

  // sets x as negative 
  println("-x = " +  (-x)) 
  // -x = -40

  // increment x by 1 or x + 1
  println("++x = " +  (++x)) 
  // ++x = 41

  // decrement x by 1 or x - 1
  println("--x = " +  (--x)) 
  // --x = 40

  // inverts the boolean value
  println("!b = " +  (!b)) 
  // !b = false
}
```

:::note
Increment `++` and decrement `--` operators can be used as **prefix** or **suffix**.
:::

If we use them as prefix, the operator will apply before expression is executed, and if we use them as suffix the operator will apply after the expression is executed.

### Logical Operators
These are AND `&&` OR `||` NOT `!`.

|        Name         |             Description                 | 
|:-------------------:|:---------------------------------------:|
|        AND          |       Returns true if both are true     |
|        OR           |       Returns true if either is true    | 
|        NOT          |           Reverses the result           | 

```kotlin
fun main(args: Array<String>) {
  var x:Boolean = true
  var y:Boolean = false

  // true AND false
  println("x && y = " +  (x && y)) 
  // x && y = false

  // true OR false
  println("x || y = " +  (x || y)) 
  // x || y = true

  // NOT false
  println("!y = " +  (!y)) 
  // !y = true
}
```

### Bitwise Operations
Here is a list of helper functions to perform bitwise operations.

|    Function    |          Description          |             Example           | 
|----------------|:-----------------------------:|:-----------------------------:|
| shl (bits)     |       signed shift left       |            x.shl(y)           |   
| shr (bits)     |       signed shift right      |            x.shr(y)           |
| ushr (bits)    |      unsigned shift right     |           x.ushr(y)           |
| and (bits)     |         bitwise and           |            x.and(y)           |      
| or (bits)      |         bitwise or            |            x.or(y)            | 
| xor (bits)     |         bitwise xor           |            x.xor(y)           | 
| inv()          |       bitwise inverse         |            x.inx              | 

```kotlin
fun main(args: Array<String>) {
  // 60 = 0011 1100
  var x:Int = 60   

  // 13 = 0000 1101 
  var y:Int = 13  

  var z:Int

  // 240 = 1111 0000
  z = x.shl(2)       
  println("x.shl(2) = " +  z) 
  // x.shl(2) = 240

  // 15 = 0000 1111
  z = x.shr(2)       
  println("x.shr(2) = " +  z)
  // x.shr(2) = 15

  // 12 = 0000 1100
  z = x.and(y)       
  println("x.and(y)  = " +  z)
  // x.and(y)  = 12

  // 61 = 0011 1101
  z = x.or(y)        
  println("x.or(y)  = " +  z)
  // x.or(y)  = 61
  
  // 49 = 0011 0001
  z = x.xor(y)       
  println("x.xor(y)  = " +  z)
  // x.xor(y)  = 49

  // -61 = 1100 0011
  z = x.inv()        
  println("x.inv()  = " +  z)
  // x.inv()  = -61
}
```
### Additional Learning Materials
🔗 [Tutorials Point](https://www.tutorialspoint.com/kotlin/kotlin_operators.htm)
---
sidebar_position: 3
---

# User Input

In this lesson, we'll learn about basic **input** and **output** in Kotlin. Input and output is terminology referring to the communication between a computer program and its user. Input is the user giving something to the program, while output is the program giving something to the user.

## Display string entered by user

In this example, we will take the input from user and display it in the output. Here we are using `readLine()` function to read the string entered on console.

```kotlin 
fun main(args: Array<String>) {
  print("Write anything here: ")

  val enteredString = readLine()
  println("You have entered this: $enteredString")
}

// Write anything here: hello world
// You have entered this: hello world
```

The `readLine()` function reads the input as a `String`.

## Taking input and converting it into a different type

If we want to take the input in a different type such as integer, long, double then we need to either explicitly convert the input to another type or use the java `Scanner` class.

Here we are explicitly converting the input of type String to an integer number.

```kotlin
fun main(args: Array<String>) {
  print("Write any number: ")

  val number = Integer.valueOf(readLine())
  println("The entered number is: $number")
}

// Write any number: 101
// The entered number is: 101
```

## Taking the input other than String using Scanner class
Here, we are taking the input as an integer and float using the `nextInt()` and `nextFloat()` functions respectively. Similarly we can use `nextLong()`, `nextDouble()` and `nextBoolean()` methods to take long, double and boolean inputs respectively.

```kotlin
import java.util.Scanner

fun main(args: Array<String>) {
  //creating Scanner object
  val read = Scanner(System.`in`)

  //taking integer input
  println("Enter an integer number: ")
  var num1 = read.nextInt()

  //taking float input
  println("Enter a float number: ")
  var num2 = read.nextFloat()

  //displaying input numbers
  println("First Input Number: " + num1)
  println("Second Input Number: " + num2)
}

// Enter an integer number: 
// 99
// Enter a float number: 
// 10.55
// First Input Number: 99
// Second Input Number: 10.55
```

## Additional Learning Materials
🔗 [Beginners Book](https://beginnersbook.com/2018/09/kotlin-input-from-user/) <br/>
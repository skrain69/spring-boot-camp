---
sidebar_position: 5.1
---

# Booleans
Boolean is used to represent a value that is either `true` or `false`. It is useful for decision-making statements. The boolean function `.and` performs logical `AND` operation between two boolean values. It returns true if both are true. The boolean function `.or` performs logical `OR` operation, which returns `true` if either is `true`. Lastly, the boolean function `not` returns the inverse of the boolean. It returns `true` if the boolean is `false`, and vice versa.

```kotlin
fun main(args: Array<string>) {
  var t = true;
  var f = false;

  // true AND true
  println("t.and(t): "+t.and(t)); 
  // true

  // true AND false
  println("t.and(f): "+t.and(f)); 
  // false

  // false AND true
  println("f.and(t): "+f.and(t)); 
  // false

  // false AND false
  println("f.and(f): "+f.and(f)); 
  // false

  // true OR true
  println("t.or(t): "+t.or(t)); 
  // true

  // true OR false
  println("t.or(f): "+t.or(f)); 
  // true

  // false OR true
  println("f.or(t): "+f.or(t)); 
  // true

  // false OR false
  println("f.or(f): "+f.or(f)); 
  // false 

  // NOT true 
  println("not(t): "+t.not()); 
  // false

  // NOT false
  println("not(f): "+f.not()); 
  // true
}
```

## .xor
It performs the logical `xor` operation, which becomes false if the boolean values are the same. 

```kotlin
fun main(args: Array<string>) {
  var t = true;
  var f = false;

  // true XOR true
  println("t.xor(t): "+t.xor(t)); 
  // false

  // true XOR false
  println("t.xor(f): "+t.xor(f)); 
  // true

  // false XOR true
  println("f.xor(t): "+f.xor(t)); 
  // true

  // false XOR false
  println("f.xor(f): "+f.xor(f)); 
  // false
}
```

## .compareTo
The boolean function `compareTo` is used to compare one boolean with another. It returns zero if both are **equal**, negative number if it is **less than** the other value, and a positive number if it is **greater than** the other value.

```kotlin
fun main(args: Array<string>) {
  var t = true;
  var f = false;

  // true compareTo true
  println("t.compareTo(t): "+t.compareTo(t)); 
  // 0

  // true compareTo false
  println("t.compareTo(f): "+t.compareTo(f)); 
  // 1

  // false compareTo true
  println("f.compareTo(t): "+f.compareTo(t)); 
  // -1

  // false compareTo false
  println("f.compareTo(f): "+f.compareTo(f)); 
  // 0
}
```

## .equals
It returns one boolean value indicating one boolean is equal to another boolean or not.

```kotlin
fun main(args: Array<string>) {
  var t = true;
  var f = false;

  // true equals true
  println("t.equals(t): "+t.equals(t)); 
  // true
  
  // true equals false
  println("t.equals(f): "+t.equals(f)); 
  // false
  
  // false equals true
  println("f.equals(t): "+f.equals(t)); 
  // false

 // false equals false
  println("f.equals(f): "+f.equals(f)); 
  // true
}
```

## .hashCode
It returns the hashcode integer value for a boolean.

## .toString
Returns the string representation of the boolean value: `true` or `false`.

## Additional Learning Materials
🔗 [Code Vs Color](https://www.codevscolor.com/kotlin-boolean-functions#tostring-)
---
sidebar_position: 18
---

# While Loop
It is executed continuously as long as the specified condition is `true`.

 ```kotlin
 while (condition) {
  // body of the loop
}
```

When it reaches the `while` loop, it checks the given condition. If given condition is `true`, the body of the loop gets executed. Otherwise, it exits the loop.

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  var i = 5

  // prints i until it is less than 0
  while (i > 0) {
    println(i)
    // decrements i
    i--
  }
}

// 5
// 4
// 3
// 2
// 1
```
The loop continues as long as the counter variable `i` is greater than 0.

## Do While Loop
This loop will execute the code block **once**, before checking if the condition is `true`. It **directly** enters the loop and executes the code before it checks for the given condition. If the given condition is `true`, it repeats the loop as long as the given condition is `true`.

```kotlin
do {
  // body of the loop
} while (condition)
```

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  var i = 5

  // prints i until it is below 0
  do{
    println(i)
    // decrements i
    i--
  } while (i > 0)
}

// 5
// 4
// 3
// 2
// 1
```

The loop will always be executed at least once, because it checks condition **before** the body gets executed. Whereas `do while` loop checks the condition **after** the body gets executed.


## Additional Learning Materials
🔗 [Tutorials Point](https://www.tutorialspoint.com/kotlin/kotlin_while_loop.htm)
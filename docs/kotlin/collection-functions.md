---
sidebar_position: 20.1
---

# Collection Functions

## Map
It is a function on the array object, and takes a **callback function**. 

```kotlin
var names = animals.map(fun(animal: Animals): String {
  return animal.name
})
```
The **callback function** will pass each item in the `animals` array. The `map` will include all items in the array and expects the callback function to return a transformed object that will be put into the **new** array `name`. Since map just expects the callback to return any object, we can use it to create completely new objects.

```kotlin
var names = animals.map(fun(animal: Animals): String {
  return animal.name + "is a" + animal.species
})
```

:::note
Map takes an array and transforms that into a new array of the **same length**, but with each individual item transformed.
:::

## Reduce
The `reduce()` is one of the built-in function that **accumulates** the values. It starts with the **index element**, and applies the code operation from left to right. The values declared by the specific variable types are going to be of array types. The value starts from the index 0.

```kotlin
var totalAmount = amounts.reduce(fun(sum, amount): Int {
  return sum + amount
})
```

The first argument `sum` is the initial value which is 0, and we’re adding the `amount` to it. The **return value** will be passed as the `sum` in the next situation, which in turn, will add its `amount` to it and so on until we are finished.

```kotlin
import kotlin.collections.*

fun main() {
val x = listOf(12,34,56,78,90,627, 54,56,65324,65,62145)

val y = x.reduce { y, vars -> y + vars }

println("Have a Nice Day users its the first example regarding the reduce() "+y) 
// Have a Nice Day users its the first example regarding the reduce() 128541

val z = x.fold(2) { y, vars -> y + vars * 5}

println("Thank you users for spending the time please try again "+z)
// Thank you users for spending the time please try again 642707

val ab = listOf("First", "Welcome", "To", "My","Domain")

println(ab.reduce { cd, vars1 -> cd + vars1 })
// FirstWelcomeToMyDomain

val ef = listOf("second", "Please", "Try", "Again","Have", "A", "Nice", "Day", "Users","123","456","789")

println(ef.reduce { gh, vars2 -> gh + vars2 })
// secondPleaseTryAgainHaveANiceDayUsers123456789
}
```

## Additional Learning Materials
🔗 [Medium](https://medium.com/mindorks/functional-programming-in-kotlin-part-2-map-reduce-ebdb3ebaa1f6)<br/>
🔗 [Educba](https://www.educba.com/kotlin-reduce/)
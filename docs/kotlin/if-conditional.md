---
sidebar_position: 14
---

# If Conditional
If the condition inside the `if` block is evaluated to `true` then **code block A** is executed, otherwise program goes into `else` part and **code block B** is executed.

```kotlin
if (condition) {
  // code block A to be executed if condition is true
} else {
  // code block B to be executed if condition is false
}
```

```kotlin
fun main(args: Array<String>) {
  // creates an Int variable 
  val age:Int = 10

  // checks if age is above 18
  if (age > 18) {
    print("Adult")
  } else {
    print("Minor")
  }
}

// Minor
```

It can also be used as an expression, which **returns a value**. This value can be assigned to a variable.

```kotlin
fun main(args: Array<String>) {
  // creates an Int variable 
  val age:Int = 10

  // checks if age is above 18 and stores boolean value to a variable
  val result = if (age > 18) "Adult" else "Minor"
  println(result)
}

// Minor
```
:::tip
You can ommit the curly braces `{}` when `if` has only one statement.
:::

You can use `else if` condition to specify a **new condition** if the first condition is false.

```kotlin
if (condition1) {
  // code block A to be executed if condition1 is true
} else if (condition2) {
  // code block B to be executed if condition2 is true
} else {
  // code block C to be executed if condition1 and condition2 are false
}
```
```kotlin
fun main(args: Array<String>) {
  // creates an Int variable 
  val age:Int = 13

  // checks if age is above 19
  val result = if (age > 19) {
    "Adult"
  } else if ( age > 12 && age  < 20 ){
    "Teen"
  } else {
    "Minor"
  }
  
  print("The value of result: ")
  // The value of result: 
  println(result)
  // Teen
}

// The value of result: Teen
```

## Nested if Expression
It allows to put an if expression inside another if expression. This is called **nested** if expression.
```kotlin
if(condition1) {
  // code block A to be executed if condition1 is true
  else if( (condition2) {
    // code block B to be executed if condition2 and condition1 are both true
  } else {
    // code block C to be executed if condition2 is false
  }
} else {
    // code block D to be executed if condition1 is false
}
```
```kotlin
fun main(args: Array<String>) {
  // creates an Int variable 
  val age:Int = 20 

  // checks if age is above 12 and stores boolean value to a variable
  val result = if (age > 12) {
    if ( age > 12 && age  < 20 ){
      "Teen"
    } else {
      "Adult"
    }
  } else {
    "Minor"
  }

  print("The value of result: ")
  // The value of result:
  println(result)
  // Adult
}

// The value of result: Adult
```

## Additional Learning Materials
🔗 [Tutorials Point](https://www.tutorialspoint.com/kotlin/kotlin_if_else_expression.htm)
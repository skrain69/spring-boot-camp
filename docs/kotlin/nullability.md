---
sidebar_position: 6
---

# Nullability

It is the ability to catch null-pointer errors at compile time. It is a **null safety** language aimed to eliminate the `NullPointerException` or null reference from the code. In Kotlin, we need to explicitly declare that a variable can be assigned `null` as a value. We do this by including a question mark `?` immediately after the type declaration.

```kotlin
var message:String = "Hello"

// compilation error
message = null  

var possiblyNull:String? = "Hello"

// no error
possiblyNull = null  
```
## Other examples

```kotlin
var students:Int = null // this does not work, since Int cannot be null
var students:Int? = null // this works since the type is now Int or null
```
Kotlin provides a number of other structures using the `?` to check for and handle null values.

### Additional Learning Materials
🔗 [Android Development: Lecture Notes](https://info448-s17.github.io/lecture-notes/kotlin.html)
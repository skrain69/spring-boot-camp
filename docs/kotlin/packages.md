---
sidebar_position: 22
---

# Packages
A source file may start with a **package declaration**. If you are familiar with Java, you know that Java uses packages to group related classes. For example, the `java.util` package has a number of useful utility classes. Packages are declared with the `package` keyword, and any file with a package declaration at the beginning can contain declarations of classes, functions, or interfaces.

```kotlin
package org.example

fun printMessage() { /*...*/ }
class Message { /*...*/ }

// ...
```
All the contents, such as classes and functions, of the source file are included in this package. 

:::note
The only real difference with Java is that the package name doesn’t have to match the name of the file's directory.
:::

We have declared a package `com.chikekotlin.projectx` using the `package` keyword. Also, we declared a class `MyClass` inside this package.

```kotlin
package com.chikekotlin.projectx

class MyClass
```

Now, the fully qualified name for the class `MyClass` is `com.chikekotlin.projectx.MyClass`.

```kotlin
package com.chikekotlin.projectx

fun saySomething(): String {
  return "How far?"
}
```

In the code above, we created a top-level function. Similarly, the fully qualified name for the function `saySomething()` is `com.chikekotlin.projectx.saySomething.`.

## Additional Learning Materials
🔗 [Kotlin Quick Reference](https://kotlin-quick-reference.com/95-R-imports-packages.html)<br/>
🔗 [Kotlin from Scratch](https://code.tutsplus.com/tutorials/kotlin-from-scratch-packages-basic-functions--cms-29445)
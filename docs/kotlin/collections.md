---
sidebar_position: 8
---

# Collections
A collection usually contains a number of objects (may also be zero) of the same type. Objects in a collection are called elements or items. They are used to store group of related objects in a single unit. By using collections, we can **store**, **retrieve**, **manipulate**, and **aggregate** data. Categories are **immutable**, and **mutable**.

## Immutable Collection
Also known as Collection -- supports **read-only** functionalities. 

|   Collection Type    |             Methods             | 
|----------------------|:-------------------------------:|
| List                 |            listOf()             |     
| Map                  |            mapOf()              |  
| Set                  |            setOf()              |   

## Mutable Collection
It supports both **read and write** functionalities. 

|   Collection Type    |                 Methods                  | 
|----------------------|:----------------------------------------:|
| List                 |     arrayListOf() mutableListOf()        |     
| Map                  |   HashMap hashMapOf() mutableMapOf()     |  
| Set                  |       hashSetOf() mutableSetOf()         |  

## List 
It stores elements in a specified order and provides indexed access to them. Indices start from zero – the index of the **first element** – and go to last element which is the `(list.size - 1)`.

```kotlin 
// creates list
val numbers = listOf("one", "two", "three", "four")

// gets the size of the list
println("Number of elements: ${numbers.size}") 
// Number of elements: 4

// gets 3rd element whose index is 2
println("Third element: ${numbers.get(2)}") 
// Third element: three

// gets 4th element whose index is 3
println("Fourth element: ${numbers[3]}") 
// Fourth element: four

// gets index of element "two"
println("Index of element \"two\" is ${numbers.indexOf("two")}") 
// Index of element "two" is 1
```
List elements (including nulls) can duplicate: a list can contain any number of equal objects or occurrences of a single object. Two lists are considered equal if they have the **same size** and **structurally equal** elements at the same positions.

Add or remove an element at a specific position using `mutableListOf()`.
```kotlin
// creates mutable list
val numbers = mutableListOf(1, 2, 3, 4)

// adds 5 to the list
numbers.add(5)

// removes element at index 1
numbers.removeAt(1)

// changes value at index 0 to 0
numbers[0] = 0

// shuffles list
numbers.shuffle()

// prints updated list
println(numbers) 
// [0, 5, 3, 4]
```

:::caution
An array's size is defined upon initialization and is never changed-- while a list doesn't have a predefined size.
:::

## Set
It stores unique elements. Their order is generally undefined. `null` elements are unique as well and a set can contain only one null. Two sets are equal if they have the same size, and for each element of a set there is an equal element in the other set.

```kotlin
// creates set
val numbers = setOf(1, 2, 3, 4)

// gets the size of the set
println("Number of elements: ${numbers.size}") 
// Number of elements: 4

// checks if 1 is in the list
if (numbers.contains(1)) println("1 is in the set") 
// 1 is in the set

// creates another set
val numbersBackwards = setOf(4, 3, 2, 1)

// checks if the sets are equal regardless of order
println("The sets are equal: ${numbers == numbersBackwards}") 
// The sets are equal: true
```

The default implementation of Set – LinkedHashSet – preserves the order of element insertion.

```kotlin
// LinkedHashSet is the default implementation
// creates set
val numbers = setOf(1, 2, 3, 4)  

// creates another set
val numbersBackwards = setOf(4, 3, 2, 1)

// checks if the first elements of numbers and numbersBackwards are the same
println(numbers.first() == numbersBackwards.first()) // false

// checks if the first element of numbers is equal to the last element of numberBackwards
println(numbers.first() == numbersBackwards.last()) // true
```

## Map
```kotlin
// creates map
val numbersMap = mapOf("key1" to 1, "key2" to 2, "key3" to 3, "key4" to 1)

// gets the keys
println("All keys: ${numbersMap.keys}") 
// All keys: [key1, key2, key3, key4]

// gets the values
println("All values: ${numbersMap.values}") 
// All values: [1, 2, 3, 1]

// gets the value of key2
if ("key2" in numbersMap) println("Value by key \"key2\": ${numbersMap["key2"]}")  
// Value by key "key2": 2

// checks if the map contains a value of 1
if (1 in numbersMap.values) println("The value 1 is in the map")  
// The value 1 is in the map

// checks if the map contains 1
if (numbersMap.containsValue(1)) println("The value 1 is in the map")   
// The value 1 is in the map
```
:::note
Two maps containing the equal pairs are equal regardless of the pair order.
:::

Add a new key-value pair or update the value associated with the given key using `mutableMapOf`.
```kotlin
val numbersMap = mutableMapOf("one" to 1, "two" to 2)

// adds 3 to key three
numbersMap.put("three", 3)

// changes value of key one to 11
numbersMap["one"] = 11

println(numbersMap) 
// {one=11, two=2, three=3}
```

:::note
1. **List** is an ordered collection with access to elements by indices – integer numbers that reflect their position. Elements can occur more than once in a list.
2. **Set** is a collection of unique elements. It reflects the mathematical abstraction of set-- a group of objects without repetitions. Generally, the order of set elements has no significance.
3. **Map** (also known as **dictionary**) is a set of key-value pairs. Keys are unique, and each of them maps to exactly one value. The values can be duplicates. Maps are useful for storing logical connections between objects.
:::

## Additional Learning Materials
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/collections-overview.html#collection) 
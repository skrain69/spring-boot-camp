---
sidebar_position: 3
---

# Slack 📨

Slack is a messaging app for business that connects people to the information they need. By bringing people together to work as one unified team, Slack transforms the way organizations communicate.

## Getting Started

A Slack workspace is a single place for your team and your work. If you’ve recently joined one, this guide will help you get set up in Slack so you can get started. Before you dive in, make sure you’ve [downloaded the desktop app](https://slack.com/intl/en-ph/downloads/windows) and you’re able to [sign in to your workspace](https://slack.com/intl/en-ph/help/articles/212681477-Sign-in-to-Slack).

### How to use Slack

<iframe width="950" height="600" src="https://www.youtube.com/embed/812kbhp0WdY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Fill out your profile

Your profile will help the others you work with in Slack know who you are.

![Slack](/img/slack.png "Edit Profile")

1. From your desktop, click the default profile picture in the top right, then Edit profile from the menu. 
2. Let people know what you do and add a profile photo.
3. Click Save Changes.

### Configure your notifications



Your [notifications in Slack](https://slack.com/intl/en-ph/help/articles/360025446073-Guide-to-Slack-notifications) will alert you of activity in your workspace. You can decide what messages will trigger notifications for you and set other details from your preferences.

![Slack](/img/slack-preferences.png "Preferences")

1. From your desktop, click your profile picture in the top right. 
2. Select Preferences from the menu to open your notification preferences.
3. Choose the activity you’d like to get notified about, then configure any additional notification preferences you’d like.

Visit [Configure your Slack notifications](https://slack.com/intl/en-ph/help/articles/201355156-Configure-your-Slack-notifications) to learn about all of the available notification preferences in Slack.

### Learn how to send messages
In Slack, you can send messages to other people you work with. If you need to communicate with a group of people, send a message in a [channel](https://slack.com/intl/en-ph/help/articles/360017938993-What-is-a-channel). To have a one-to-one conversation with someone, send them a [direct message](https://slack.com/intl/en-ph/help/articles/212281468-What-is-a-direct-message).

![Slack](/img/slack-new-message.png "New Message")
To start a new message, click the ✏️compose button in the top left.

:::info Note
If you’ve been invited to a workspace as a [guest](https://slack.com/intl/en-ph/help/articles/202518103-Understand-guest-roles-in-Slack), you can only send messages in channels you have access to, and DM people in the same channels as you.
:::

### Learn about other Slack features

After setting up Slack, you could start and explore other features.

- Visit [How to use Slack: your quick start guide](https://slack.com/intl/en-ph/help/articles/360059928654-How-to-use-Slack--your-quick-start-guide) for an overview of the basics.
- Explore our [Slack 101](https://slack.com/intl/en-ph/help/categories/360000049063#slack-101) and [video tutorials](https://slack.com/intl/en-ph/help/articles/360059976673-Slack-video-tutorials) to learn how to work in Slack.
- Use the keyboard shortcut `F1` to open **Help** in Slack and find answers quickly.

## Additional Learning Materials
🔗 [Slack Help Center](https://slack.com/intl/en-ph/help) 
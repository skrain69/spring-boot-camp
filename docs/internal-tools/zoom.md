---
sidebar_position: 5
---

# Zoom 🔭

Zoom is an online application that provides videotelephony and online chat services through a cloud-based peer-to-peer software platform and is used for teleconferencing, telecommuting, distance education, and social relations. It is a collaborative place for users to find solutions, ask questions, and connect with peers.

## Getting Started

### How to join a Zoom meeting

<iframe width="950" height="600" src="https://www.youtube.com/embed/hIkCmbvAHQQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### How to use Zoom's virtual backgrounds

<iframe width="950" height="600" src="https://www.youtube.com/embed/3Zq-b51A3dA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### How to share your screen in Zoom

<iframe width="950" height="600" src="https://www.youtube.com/embed/YA6SGQlVmcA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### How to use your Zoom dashboard

<iframe width="950" height="600" src="https://www.youtube.com/embed/Rb2fT-N_VCA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [Zoom](https://support.zoom.us/hc/en-us?_ga=2.126629131.70794101.1633068727-1598434438.1627761423)


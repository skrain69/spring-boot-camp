---
sidebar_position: 4
---

# Discord 🎮

Discord is a VoIP, instant messaging and digital distribution platform. Users communicate with voice calls, video calls, text messaging, media and files in private chats or as part of communities called "servers". 

It is a popular group-chatting app and was originally made to give gamers a place to build communities. Today, it has branched out to include communities from all over the internet, ranging from writers to artists to study groups. It is known to be a fun place where users could work, play and socialize online. 

## Getting Started

<iframe width="950" height="600" src="https://www.youtube.com/embed/OIqyPJQAgT4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [Discord](https://support.discord.com/hc/en-us/articles/360045138571-Beginner-s-Guide-to-Discord)
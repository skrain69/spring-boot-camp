---
sidebar_position: 6
---

# Airmeet 🎧

Airmeet is an online venue for your events, designed to deliver real engagement and best-in-class interactions. It allows participants to interact on virtual tables through videos, table chats, and speakers to broadcast live sessions. 

## Getting started

### Meetup format
<iframe width="950" height="600" src="https://www.youtube.com/embed/9gfbPTwM73g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Conference format
<iframe width="950" height="600" src="https://www.youtube.com/embed/ETaz-I-CQ94" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [Airmeet](https://help.airmeet.com/support/solutions/articles/82000443337-what-is-airmeet-)
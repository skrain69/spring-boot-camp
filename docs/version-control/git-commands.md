---
sidebar_position: 1
---

# Git Commands

Check out the most common commands you'll need to know how to use Git 🥳

### 😋 Cloning an existing project
Cloning or saving a local copy of a repository.
 ```git
 git clone https://username@bitbucket.org/teamsinspace/documentation-tests.git
 ```
👌 Or you can copy an actual repository if it is on the same computer.
```git
git clone /home/geyer/GitRepos/Git
```
👌 Or you can do the same from another computer.
```git
git clone geyer@ssh.stat.umn.edu:/home/geyer/GitRepos/Git
```

### 😋 Opening a project in VSCode
Opens a project or repository in VSC. 
```git
code .
```

### 😋 Creating a branch 
Creates a new branch.
```git
git branch newBranch // creates a new branch
git checkout newBranch // switches to the branch named "newBranch"
git checkout -b newBranch // creates and switches to the newly created branch
``` 

### 😋 Creating a folder
Creates a new folder from the command line.
```git
mkdir newFolder
```

### 😋 Changing directory
Changes directory or opens a folder from the command line.
```git
cd newFolder
```

### 😋 Creating a file
Creates a new file from the command line.
```git
touch index.html
```

### 😋 Adding changes to the index (staging area)
```git
git add readme.md // adds a specific file
git add . // adds all changes
```

### 😋 Commiting changes and composing commit message
```git 
git commit -m "added readme.md"
git commit -a // commits all
git commit --all // commits all
```

### 😋 Pushing changes
 Pushes existing changes to `main` branch, from the command line.
```git 
git push origin main 
```

### 😋 Pulling changes
Pulls existing changes in the repository that you do not have locally.
```git
git pull origin main
```

### 😋 Merging 
Merges two branches.
```git 
git checkout main // switches to main
git merge newBranch // merges main and newBranch
```

### 😋 Git status
Displays paths that have differences between the index file and the current HEAD commit, paths that have differences between the working tree and the index file, and paths in the working tree that are not tracked by git.
```git
git status
```

### 😋 Git diff
Shows the differences between the working tree and the index (staging area).
```git
git diff
```

### 😋 Git log
Describes all commits.
```git
git log
```

Feel free to check out the other git commands. Happy coding! 😄
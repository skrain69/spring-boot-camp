---
sidebar_position: 2
---

# Big O Notation 🧮

![Big O Notation](/img/big-o.png "Chart")
**Big-O** notation is the language we use for talking about how long an algorithm takes to run (time complexity) or how much memory is used by an algorithm (space complexity). Big-O is an asymptotic shorthand mathematical notation that defines the upper bound of an algorithm and can express the best, worst, and average-case ***running time*** of an algorithm. 

Runtime is not the actual time required to execute code. If it is measured directly, we could express the speed in seconds.
Since we are measuring how quickly the runtime grows, we represent it regarding something else known as Big-O notation where the input size in ‘n’.
For any algorithm having a function g(n), where n is the time to execute the algorithm, we can say the algorithm is O(g(n)).

The algorithms are classified from the best-to-worst performance as follows -

👉 Logarithmic algorithm – **O(logn)** <br/>
👉 Linear algorithm – **O(n)**<br/>
👉 Superlinear algorithm – **O(nlogn)**<br/>
👉 Polynomial algorithm – **O(n<sup>c</sup>)**<br/>
👉 Exponential algorithm – **O(c<sup>n</sup>)**<br/>
👉 Factorial algorithm – **O(n!)**

:::info Note
The fastest possible runtime time to execute an algorithm is **O(1)**, commonly known as [Constant Running Time](https://www.geeksforgeeks.org/analysis-algorithms-big-o-analysis/#:~:text=The%20fastest%20possible%20running%20time,algorithm%2C%20but%20it's%20rarely%20achievable.).
:::

## Constant time

**O(1)** represents an algorithm that always takes the same time regardless of the size of the input.

Let us look at the following example:

```kotlin
int n = 5;
println("Your input is: " + n);
```
The output of the code is ***Your input is: 5*** which will be printed only once on the screen. So the time complexity is **O(1)**. That means every time a constant amount of time is required to execute the code irrespective of what system configuration you are having.

## Polynomial algorithm

**O(n2)** represents an algorithm whose complexity is directly proportional to the square of the input size.

For example, imagine there are 50 students in a classroom, and one of them has your book with him. You want to find it out. You go and ask the first student in the class if he has it.

You also ask him about the rest 49 of students if they have the book. In that way, you find out who has your book. This is known as **O(n2)**.

Let’s have a look at this example:

```kotlin
for (int i = 1; i <= n; i++) {
    for(int j = 1; j <= n; j++) {
        println("Your output is " + i + " and " + j);
    }
}
```
If **n** is 5, then the algorithm will run 52 = 25 times. Now, if we nest another for loop, this would become a cubic algorithm(**O(n<sup>3</sup>)**).

## Superlinear algorithm

**O(logn)** describes an algorithm whose [complexity increases logarithmically](https://towardsdatascience.com/logarithms-exponents-in-complexity-analysis-b8071979e847) as the input size increases. The curve peaks at the beginning and slowly becomes flat as the size of the data increases.

Consider the same example, now you divide the class into two groups and then ask if the book is on the left side or the right side. Then you one group and again subdivide it into two and ask again, and you keep on repeating that until you find that one student who has your book. This is what you mean by **O(logn)**.

Let’s have a look at this example:

```kotlin
for (int i = 1; i < n; i = i * 2){
    println("Your output is " + i);
}
```
If **n** is 8, the output will be:

```kotlin
Your output is 2
Your output is 4
Your output is 8
```
So, the algorithm ran log(8) = 3 times.

## Linear algorithm

**O(n)** is directly proportional to the number of inputs and grows linearly. If you go on asking each student individually till you find out who has your book, this is what we call it as **O(n)**.

Let us have a look at the following example:

```kotlin
for (int i = 0; i < n; i++) {
    println("Your output is " + i);
}
```
This loop runs for **n** times.

## Exponential algorithm

**O(2n)** represents an algorithm which doubles with every additional input. Let us have a look at this example:

```kotlin
for (int i = 1; i <= Math.pow(2, n); i++){
    println("Your output is " + i);
}
```

If **n** is 5, this algorithm will run 2<sup>5</sup> = 32 times.

## Factorial algorithm 

**O(n!)** represents an algorithm whose runtime is proportional to the factorial of the input size.

Let us have a look at this simple example:

```kotlin
for (int i = 1; i <= factorial(n); i++){
    println("Your output is " + i);
}
```
If **n** is 5, this algorithm will run 5! = 120 times.

## Examples of Big-O notations:

🖊 **O(1):** - Determining if a number is even or odd. <br/>
🖊 **O(logn):** - Find an item in a sorted array with a binary search.<br/>
🖊 **O(n):** - Find an item in an unsorted list.<br/>
🖊 **O(nlogn):** - Merge sort, quick sort, and heap sort.<br/>
🖊 **O(n<sup>2</sup>):** - Traversing a simple 2D array.

:::info Note
The Big-O notation is very much required to judge the speed of an algorithm with **O(n)** and **O(logn)** giving us the best performance in executing an algorithm.
**O(logn)** has the same functionality as **O(n)**, but the implementation is completely different and provides us with a better performance with larger inputs.
:::

## Additional Learning Materials
🔗 [chercher.tech](https://chercher.tech/kotlin/big-o-notation-kotlin)<br/>
🔗 [Pamela Lovett](https://www.linkedin.com/pulse/big-o-notation-simple-explanation-examples-pamela-lovett/)

---
sidebar_position: 1
---

# Databases 💾 

A database refers to a collection of logically related information organized so that it can be easily accessible, managed, and updated. Databases are generally accessed electronically from a computer system and are usually controlled by a database management system (DBMS). The database administrator (DBA) is the individual responsible for managing the databases, including database security, access control, backup, and disaster recovery.

## What is Data?

Before we get into the concept of a database, we should first understand what data is. Put simply, data are pieces of information or facts related to the object being considered. For example, examples of data relating to an individual would be the person’s name, age, height, weight, ethnicity, hair color, and birthdate. Data is not limited to facts themselves, as pictures, images, and files are also considered data.

There are a few key terms that would be useful to help one understand data more, particularly the relation between data and databases.

**Fields**: Within a database, a field contains the most detailed information about events, people, objects, and transactions.

**Record**: A record is a collection of related fields.

**Table**: A table is a collection of related records with a unique table name

**Database**: A database is a collection of related tables.

## What is the Role of Databases in an Enterprise?

Enterprises typically make use of both internal databases and external databases. Internal databases typically include operational databases and data warehouses. The former, operational databases, refer to databases that are actively used in the operations of the business, such as accounting, sales, finance, and HR.

Data warehouses contain data collected from several sources, and the data contained within are generally not used for routine business activities. Instead, data warehouses are usually used for business intelligence purposes. External databases refer to databases external to an organization and are generally accessed over the Internet and are owned by other organizations. An example of an external database is the SEC database.

## Components of a Database

The five major components of a database are:

1. Hardware
Hardware refers to the physical, electronic devices such as computers and hard disks that offer the interface between computers and real-world systems.

3. Software
Software is a set of programs used to manage and control the database and includes the database software, operating system, network software used to share the data with other users, and the applications used to access the data.

3. Data
Data are raw facts and information that need to be organized and processed to make it more meaningful. Database dictionaries are used to centralize, document, control, and coordinate the use of data within an organization. A database is a repository of information about a database (also called metadata).

4. Procedures
Procedures refer to the instructions used in a database management system and encompass everything from instructions to setup and install, login and logout, manage the day-to-day operations, take backups of data, and generate reports.

5. Database Access Language
Database Access Language is a language used to write commands to access, update, and delete data stored in a database. Users can write commands using Database Access Language before submitting them to the database for execution. Through utilizing the language, users can create new databases, tables, insert data, and delete data.
![Database](/img/databases.png "Components of Databases")

## What is a Database Management System (DBMS)?

A Database Management System (DBMS) is a well-known term in data analysis. It refers to a collection of programs that enable users to access databases and manipulate, maintain, report, and relate data. A DBMS is often used to reduce data redundancy, share data in a controlled way, and reduce data integrity problems. DBMS is not an information system but is simply software.

The relational model, which saves data in table formats, is the most widely used DBMS. The relational DBMS organizes information into rows, columns, and tables, making it easier to find relevant information. Relational databases are popular because they are easy to extend, and new data categories can be added after the original database is created without large amounts of modification.

The Structured Query Language (SQL) is considered the standard user and application program interface for a relational database, and all relational DBMS software supports SQL. Examples include FileMaker Pro, Microsoft Access, Microsoft SQL Server, MySQL, and Oracle.


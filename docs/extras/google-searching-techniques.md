---
sidebar_position: 2
---

# Google Searching Techniques 
As software engineers, we spend a lot of time searching for things on the internet. Therefore, it makes sense to spend time learning some little known search engine tricks that will boost your search productivity. You can find basic and advanced tricks here. Keep reading! 😋

## Use Boolean Language 😉
Using Boolean logic such as `AND` and `OR` should be provided to Google in upper case so that Google can identify them as operators to be utilized in the search algorithm as opposed to part of the search text.

Searching for a recipe for apple pie?
```kotlin
recipe AND “apple pie” // specify
```

In regular searches, google defaults to AND anyway, so it doesn’t matter make much difference. But it can be quite useful when combined with other operators.

If you want to search for a specific group of words, such as when looking for a specific quotation, enclose the words in `""` so that Google will search for that exact text.

Searching for who said “I have not yet begun to fight” 
```kotlin
“I have not yet begun to fight” // use "" for exact search
```
## Wildcards 🃏
Wildcards simply mean, **match** any word or phrase. Quite useful if you forgot part of the word or phrase you wanted to search. There are represented by an `*`.

:::tip
Knowing these little tricks and having the ability to combine them is quite powerful.
:::

## Search Operators 🔍
There are Google **search operators** that you can add to your searches to limit the searching that Google will perform.

### site:
If you would like to limit your search to a specific web site you can simply insert the operator `site`

```kotlin
site:accuwebhosting.com // limits the search to only the **accuwebhosting.com** web site
```

### intitle:
It limits the search to web page **titles** only. These search limitations can be combined with the `AND` and `OR` operators to further control your search results. 

### inurl:
It includea results whose URL contain the word or phrase. This is extremely helpful when you want to look for something in an online documentation site that’s hard to navigate.

```kotlin
inurl:developer.linkedin.com “pagination”
```

When combined with **wildcards**

```kotlin
site:*.facebook.com “access tokens” “api”

inurl:admin-sdk site:*.google.com “reseller api”
```


### filetype:
It searches for a document in a particular file type.

```kotlin
filetype:pdf "craftsman owners manual"
filetype:docx “metaprogramming”
```

## Use Google's Cache for Lost Sites 📦
Each time a site is visited, the site is saved in Google’s **cache storage**.

To see the site, simply click the green drop-down area next to the site name and select “**cached**”. Google will show you the web site as it appears in cache storage. Or just enter the **operator** followed by the web site you want to see – `cache: www.irs.gov`
---
sidebar_position: 4
---

# Code Refactoring
Most Common Code Refactoring Techniques:
## Red-Green Refactoring 🚦
The most popular and widely used code refactoring technique in the Agile software development process. This technique follows the **“test-first”** approach.<br/>

**Red** 🚨<br/>
The first step starts with writing the failing “red-test”. You stop and check what needs to be developed.<br/>

**Green** 🟢<br/>
In the second step, you write the simplest enough code and get the development pass “green” testing.<br/>

## Refactoring By Abstraction 🤾‍♀️
This technique is mostly used by developers when there is a need to do a large amount of refactoring. This technique is used to reduce the redundancy of the code. Pull-Up/Push-Down method is the best example of this approach.<br/>

**Pull-Up method** 🏋️<br/>
It pulls code parts into a superclass and helps in the elimination of code duplication.<br/>

**Push-Down method** 👇<br/>
It takes the code part from a superclass and moves it down into the subclasses.<br/>

In this technique, we build the abstraction layer for those parts of the system that needs to be refactored and the counterpart that is eventually going to replace it. <br/>

## Composing Method ✍️
**Extraction** ⌨️<br/>
We break the code into smaller chunks to find and extract fragmentation. After that, we create separate methods for these chunks, and then it is replaced with a call to this new method. Extraction involves class, interface, and local variables. <br/>

**Inline** 🗑️<br/>
This approach removes the number of unnecessary methods in our program. We find all calls to the methods, and then we replace all of them with the content of the method. After that, we delete the method from our program.<br/>

## Simplifying Methods 📝
**Conditional Expressions Refactoring** 📏<br/>
You need to simplify the logic in your code to understand the whole program. Some of the ways are: consolidate conditional expression and duplicate conditional fragments, decompose conditional, replace conditional with polymorphism, remove control flag, replace nested conditional with guard clauses, etc.

**Method Calls Refactoring** 📞<br/>
We make method calls simpler and easier to understand. Examples are: adding, removing, and introducing new parameters, replacing the parameter with the explicit method and method call, parameterize method, making a separate query from modifier, preserve the whole object, remove setting method, etc. 

## Moving Features Between Objects 🚚
In this technique, we create new classes, and we move the functionality safely between old and new classes. We **hide** the implementation details from public access. 

Examples are: move a field, extract class, move method, inline class, hide delegate, introduce a foreign method, remove middle man, introduce local extension, etc.

## Preparatory Refactoring 🌱
You save yourself with future technical debt if you notice that the code needs to be **updated** during the earlier phases of feature development. 

## User Interface Refactoring 💻
You can make **simple changes** in UI and refactor the code. For example: align entry field, apply font, reword in active voice indicate the format, apply common button size, and increase color contrast, etc.

:::tip 
A clean and well-organized code is always easy to understand and maintain.
:::

You need to consider the code refactoring process as cleaning up the orderly house. Unnecessary clutter in a home can create a chaotic and stressful environment. The same goes for written code.
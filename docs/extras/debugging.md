---
sidebar_position: 3
---

# Debugging

The process of finding bugs or errors and fixing them in any application or software is called **debugging**.

## Debugging Process
To make the software programs **bug-free**, this process should be done before releasing them into the market.

**Identifying the error** ⚠️<br/>
It saves time and avoids the errors at the user site. Identifying errors at an earlier stage helps to minimize the number of errors and wastage of time.<br/>

**Identifying the error location** 📍<br/>
The exact location of the error should be found to fix the bug faster and execute the code.<br/>

**Analyzing the error** 🤔<br/>
To understand the type of bug and reduce the number of errors, we need to analyze the error. Solving one bug may lead to another bug that stops the application process.<br/>

**Prove the analysis** 🧾<br/>
Once the error has been analyzed, we need to prove the analysis. It uses a test automation process to write the test cases through the test framework.<br/>

**Cover the lateral damage** 🩹<br/>
The bugs can be resolved by making the appropriate changes and move onto the next stages of the code or programs to fix the other errors.<br/>

**Fix and Validate** 👨‍🔧<br/>
This is the final stage to check all the new errors, changes in the software or program and executes the application.

## Debugging Techniques
To perform the debugging process easily and efficiently, it is necessary to follow some techniques.<br/>

🛠️ **Debugging by brute force**<br/>
It is the most commonly used technique. This is done by taking memory dumps of the program which contains a large amount of information with intermediate values and analyzing them, but analyzing the information and finding the bugs leads to a waste of time and effort.<br/>

🛠️ **Induction strategy**<br/>
It includes the location of relevant data, the organization of data, the devising hypothesis (provides possible causes of errors), and the proving hypothesis.<br/>

🛠️ **Deduction strategy**<br/>
It includes identification of possible causes of bugs or hypothesis elimination of possible causes using the information refining of the hypothesis( analyzing one-by-one).<br/>

🛠️ **Backtracking strategy**<br/>
It is used to locate errors in small programs. When an error occurs, the program is traced one step backward during the evaluation of values to find the cause of bug or error.<br/>

🛠️ **Debugging by testing**<br/>
It is the conjunction with debugging by induction and debugging by deduction technique. The test cases used in debugging are different from the test cases used in the testing process.<br/>
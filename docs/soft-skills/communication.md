---
sidebar_position: 1
---

# Communication ☎️

![Communication](/img/communication.png "Communication")
Communication is a very effective soft skill that should not be neglected. A key part of being part of a development team is being willing to speak up in meetings, either with the staff or with customers. 

Some of the things that you should keep in mind while communicating are the following:

- **Listen** 🦻 – Listening is an important part of communication. Before you start giving your point of view on the app development process or errors, first listen to your team members, and try to get on the same page when finding the best solution. Having the ability to listen also allows programmers to learn new things and expand their knowledge.

- **Don’t Interrupt** 🤐 – Interrupting generally happens when a programmer has knowledge about something that they feel can’t wait until others are finished talking. This is not a good sign for communication or teamwork. It’s important to wait until the other person is finished talking.

- **Speak Clearly** 🔊 – Whenever any sort of discussion is going on, whether about a new project or progress on the current project, you should speak up and clearly outline any ideas you have. Communicating your ideas in a clear manner ensures that everyone is on the same page.

:::info Note
Following these communication tips will help you have good interpersonal relationships with your colleagues.👌
:::
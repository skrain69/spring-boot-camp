---
sidebar_position: 4
---

# Clean Code with Kotlin 🧽
Before we start, it’s important to recap what clean code is. First and foremost, clean code is code that is **easy to understand**. The code should be readable and intuitive. We can achieve this by making our code concise, short, simple and expressive. We are also facing clean code when it comes with minimal ceremony and syntactic noise.

## Functions
Functions should be *small*. According to clean code, functions should be small and they should do only one thing. We should extract subroutines and give them descriptive names. This way, our code becomes like a story. Moreover, we should separate details from the main logic. It’s up to the developer to create small functions.

## No Side-Effects
Clean Code tells us to reduce side-effects. We should not make unexpected and hidden changes that are not obvious when looking at the function name. Code with side-effects is **error-prone**, hard to understand, hard to test, hard to parallelize, not cacheable and can’t be evaluated lazily. We can avoid side-effects with concepts from functional programming. This basically means to write *pure functions*.

## Expressions
Another neat feature are **single expression functions**. If a function contains only a single expression, we can leave out the curly braces `{}` and the return type. The essential logic is revealed immediately due to the reduced syntactic noise.

## Be Aware of Train Wrecks
It’s tempting to squeeze everything into a single expression. Just because you can do this, doesn’t mean that it’s a good idea. Developer’s *discipline* and awareness for readable and clean code are especially important at this point. When in doubt prefer temporary variables and subroutines. It shouldn’t be our aim to use expressions but to create **readable** code. Readability beats squeezing everything into a single line.

## Reduced Syntactical Noise
No `new` keyword required to call a constructor. No semicolon required. Types can be inferred. No need to write them down. Just write `val`. Often, no explicit cast is required. No escaping required in triple-quoted strings.

Especially *extension functions* can make your code expressive and cleaner. But be careful with **operator overloading**. It can lead to clean but also to bad code. Only use it, when the meaning of the operator is intuitive (like + with numbers, strings and dates). In other cases, prefer a method call with a descriptive and intention-revealing name.

## Naming
Names should be **intention-revealing**. Same goes for comments, formatting, boundaries, class design and so on.

Clean code and good design are no automatism with Kotlin. The developer’s discipline is still important. Use some features with sound judgment. Sometimes the “old” means are the better choice. Clean code leads to **increased readability** due to less boilerplate and syntactic noise, and **increased safety**.
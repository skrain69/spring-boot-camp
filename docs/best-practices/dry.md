---
sidebar_position: 1
---

# Don't Repeat Yourself ❌🔄

The DRY or Don't Repeat Yourself software design principle states that each small pieces of knowledge (code) may only occur exactly once in the entire system. 
It is a basic principle of software development that aims to reduce information repetition. 

According to the book 'The Pragmatic Programmer', DRY is defined as
> Every piece of knowledge must have a single, unambiguous, authoritative representation within a system.

This helps us to write scalable, maintainable and reusable code.

## Violations of DRY 

Repetition: means writing the same code or logic again and again. It will be difficult to manage the code and if the logic changes, then we have to make changes in all the places where we have written the code, thereby wasting everyone's time.

:::info Note
**Knowledge duplication** is always a DRY principle violation. <br/>
**Code duplication** doesn’t necessarily violate the DRY principle.
:::

## How to achieve DRY 

To avoid violating the DRY principle, divide your system into pieces. Divide your code and logic into smaller reusable units and use that code by calling it where you want. Don't write lengthy methods, but divide logic and try to use the existing piece in your method.

## Benefits of DRY 
Less code is good: It saves time and effort, is easy to maintain, and also reduces the chances of bugs.

:::tip 
One good example of the DRY principle is the helper class in enterprise libraries, in which every piece of code is unique in the libraries and helper classes.
:::

## DRY Example

Let us understand the DRY principle. In the example below we have a Technician class which repairs trucks and cars.

```java
public class Technician {
	public void repairTruck() {
		System.out.println("Repairing truck now");
	}
	public void repairCar() {
		System.out.println("Repairing car now");
	}
}
```
Technician class have two methods repairTruck() and repairCar() which will perform respective tasks. Now consider the case when the garage is offering you other services like changing the engine lubricant of the vehicle after repair. 

```java
public class Technician {
	public void repairTruck() {
		System.out.println("Repairing truck now");
                //Change engine lubricant
	}
	public void repairCar() {
		System.out.println("Repairing car now");
                //Change engine lubricant
	}
}
```
As you can see the change engine lubricant code is duplicated. We can put this code in a separate method and use it wherever required.

```java
public class Technician {
	public void repairTruck() {
		System.out.println("Repairing truck now");
                changeLubricant();
	}
	public void repairCar() {
		System.out.println("Repairing car now");
                changeLubricant();
	}
        public void changeLubricant() {
               //Change engine lubricant
	}
}
```







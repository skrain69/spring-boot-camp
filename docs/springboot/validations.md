---
sidebar_position: 16
---

# Validations for RESTful services

The validation is a common requirement in all the services. We will discuss Java Validation API to add validation in our beans file. When we get a request to create a user, we should validate its content. If it is invalid, we should return a proper response.

Let's see how to validate a request.

**Step 1:** Open the UserResource.java file.

**Step 2:** Add @Valid annotation. It is a Javax validation API. Its default classpath is spring-boot-starter-web.

### UserResource.java

```java
package com.javatpoint.server.main.user;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserResource {
  @Autowired
  private UserDaoService service;

  @GetMapping("/users")
  public List<User> retriveAllUsers() {
    return service.findAll();
  }

  //retrieves a specific user detail  
  @GetMapping("/users/{id}")
  public User retriveUser(@PathVariable int id) {
    User user = service.findOne(id);
    if (user == null)
//runtime exception  
      throw new UserNotFoundException("id: " + id);
    return user;
  }

  //method that delete a user resource  
  @DeleteMapping("/users/{id}")
  public void deleteUser(@PathVariable int id) {
    User user = service.deleteById(id);
    if (user == null)
//runtime exception  
      throw new UserNotFoundException("id: " + id);
  }

  //method that posts a new user detail and returns the status of the user resource  
  @PostMapping("/users")
  public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
    User sevedUser = service.save(user);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(sevedUser.getId()).toUri();
    return ResponseEntity.created(location).build();
  }
}  
```

Now we will add validations in User class on name and date of birth. Let suppose that name should have at least five characters and date of birth should be in past not in present.

**Step 3:** Open the User.java file.

**Step 4:** Add @Size(min=5) annotation just above the name variable.

**Step 5:** Add @Past annotation just above the dob variable.

### User.java

```java
package com.javatpoint.server.main.user;

import java.util.Date;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

public class User {
  private Integer id;
  @Size(min = 5)
  private String name;
  @Past
  private Date dob;

  //default constructor     
  protected User() {

  }

  public User(Integer id, String name, Date dob) {
    super();
    this.id = id;
    this.name = name;
    this.dob = dob;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  @Override
  public String toString() {
//return "User [id=" + id + ", name=" + name + ", dob=" + dob + "]";  
    return String.format("User [id=%s, name=%s, dob=%s]", id, name, dob);
  }
}  
```

**Step 6:** Open the Rest client Postman and send a POST request with new user name Tony k. It returns Status: 201 Created.

![Postman](/img/postman10.png "Validations")

Now we send another POST request. But the name should have less than five characters. It returns the Status: 400 Bad Request.

![Postman](/img/postman11.png "Validations")

When we create a RESTful services we need to think about consumer that how does the consumer know what is wrong. To resolve this problem we will add a method handleMethodArgumentNotValid() which is defined in the ResponseEntityExceptionHandler class. This is the method which is fired when a bad request occurs.

```java
protected ResponseEntity<Object> handleMethodArgumentNotValid(  MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request)   
{  
   return handleExceptionInternal(ex, null, headers, status, request);  
}  
```

**Step 7:** Copy and paste the above method in CustomizedResponseEntityExceptionHandler.java file.

**Step 8:** Override the method by adding the annotation @Override.

### CustomizedResponseEntityExceptionHandler.java

```java
package com.javatpoint.server.main;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.javatpoint.server.main.exception.ExceptionResponse;
import com.javatpoint.server.main.user.UserNotFoundException;

//defining exception handling for all the exceptions   
@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(Exception.class)
//override method of ResponseEntityExceptionHandler class  
  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
//creating exception response structure  
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));
//returning exception structure and specific status   
    return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UserNotFoundException.class)
//override method of ResponseEntityExceptionHandler class  
  public final ResponseEntity<Object> handleUserNotFoundExceptions(UserNotFoundException ex, WebRequest request) {
//creating exception response structure  
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));
//returning exception structure and specific status   
    return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(), ex.getBindingResult().toString());
//returning exception structure and specific status   
    return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
  }
}  
```

**Step 9:** Now, we send a POST request through Postman. It returns the exception structure with message Validation failed for argument and other details.

![Postman](/img/postman12.png "Validations")

It is difficult to understand the message for the user. So we will now customize the message with the string Validation Failed instead of getting message.

```java
@Override  
protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request)   
{  
ExceptionResponse exceptionResponse= new ExceptionResponse(new Date(), "Validation Failed", ex.getBindingResult().toString());  
    //returning exception structure and specific status   
   return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);  
}  
```

**Step 10:** Again send a POST request. It returns the message which we have customized.

![Postman](/img/postman13.png "Validations")

It might be useful for the consumer. Now we customize the message again and make it more error specific.

**Step 11:** Open User.java file and add an attribute message="Name should have at least 5 characters" in @Size annotation.

```java
@Size(min=5, message="Name should have at least 5 characters")  
```

**Step 12:** Again, send a POST request. It returns the more specific exception which we have specified.

![Postman](/img/postman14.png "Validations")

We can further customize the exception by following the BindingResult interface. There is a wide variety of exception messages. There are following validation classes defined in validation-api-2.0.1.Final.jar.

![Postman](/img/folder-branch.png "Validations")
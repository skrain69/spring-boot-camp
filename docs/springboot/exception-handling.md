---
sidebar_position: 14
---

# Exception Handling

## Implementing Exception Handling- 404 Resource Not Found

**Step 1:** Open Rest client Postman and select the Get method.

**Step 2:** Click on the History tab and choose the Get request.

**Step 3:** Type the URI http://localhost:8080/users/{id}. The user id should not exist.

**Step 4:** Click on the Send Button.

![Postman](/img/postman4.png "Post request")

We get the Status: 200 OK and empty body which is a successful response even though the resource does not exist. But it is not the proper response when a resource does not exist.

How to fix this? 

**Step 1:** Open the UserResource.java file.

**Step 2:** Create a UserNotFoundException. It is a checked exception.

```java
@GetMapping("/users/{id}")
public User retriveUser(@PathVariable int id)
{
  User user= service.findOne(id);
  if(user==null)
  //runtime exception
  throw new UserNotFoundException("id: "+ id);
  return user;
}
```

**Step 3:** Create UserNotFoundException class.

**Step 4:** Generate Constructors from Superclass.

![Spring Boot](/img/generate.png "Generate Constructors from Superclass")


### UserNotFoundException.java

```java
package com.javatpoint.server.main.user;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String message) {
        super(message);
    }
}  
```

**Step 5:** Open the Rest Client Postman and generate a Get response as we have done before. It shows the Status: 500 Internal Server Error.

![Postman](/img/postman5.png "Status: 500 Internal Server Error")

But the Status: 500 Internal Server Error is not the appropriate response for the resource not found. So, we will add an annotation @ResponseStatus to generate the Status: 404 Not Found.

### UserNotFoundException.java

```java
package com.javatpoint.server.main.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String message) {
        super(message);
    }
}  
```

**Step 6:** Again move to Postman and generate a Get request.

![Postman](/img/postman6.png "Get request")

We get the proper response Status: 404 Not Found when a user resource does not exist. The body of the request provided by default error handling that's why we are getting this return status back.

![HTTP](/img/whitelabel-error2.png "Error")
---
sidebar_position: 0
---

# Introduction to Spring Boot

Spring Boot is a framework built on top of Spring that simplifies Spring development. WIth Spring Boot, it is easier to create stand-alone, production-grade Spring based Applications that you can "just run". It does this by enforcing an opinionated approach to Spring application development through “convention over configuration”, default setups, and a standardized application sturcture created by a generator called [Spring Initialzr](https://start.spring.io/).

:::tip Main goal
The main goal of Spring Boot is to reduce development, developer effort, unit test, and integration test time, and increase productivity. 
:::

## Advantages

- Create stand-alone Spring applications

- Embed Tomcat, Jetty or Undertow directly (no need to deploy WAR files)

- Provide opinionated 'starter' dependencies to simplify your build configuration

- Automatically configure Spring and 3rd party libraries whenever possible

- Provide production-ready features such as metrics, health checks, and externalized configuration

- Absolutely no code generation and no requirement for XML configuration

- Minimizes writing multiple boilerplate codes (the code that has to be included in many places with little or no alteration), XML configuration, and annotations

- Increases productivity and reduces development time

- Offers a CLI tool for developing and testing the Spring Boot application.

:::caution Limitations
Spring Boot can use dependencies that are not going to be used in the application. These dependencies increase the size of the application.
:::

## Prerequisites

- Java 1.8
- Maven 3.0+
- Spring Framework 5.0.0.BUILD-SNAPSHOT
- Integrated Development Environment (IDE) 
- Basic knowledge of Spring Framework.

## Features

- Web Development
- SpringApplication
- Application events and listeners
- Admin features
- Externalized Configuration
- Properties Files
- YAML Support
- Type-safe Configuration
- Logging
- Security













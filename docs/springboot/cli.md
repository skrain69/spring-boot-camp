---
sidebar_position: 5
---

# Spring Boot CLI

Spring Boot CLI is a tool which you can download from the official site of Spring Framework. Below are the steps:

Download the CLI tool from [official site](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html) as seen below. 

![Spring URL](/img/url.png "Spring Official Site")
![Spring Boot CLI](/img/cli.png "Spring Boot CLI")

After downloading, extract the zip file. It contains a bin folder, in which spring setup is stored. We can use it to execute Spring Boot application.

:::info Note 
You can rename your folder after extracting the zip file. It's all up to you!
:::

![Bin file](/img/spring-2.5.5_bin.png "Bin file")

No specific environment variables are required to run the CLI, however, you may want to set SPRING_HOME or the FOLDER_NAME of your choice to point to a specific installation. You should also add SPRING_HOME/bin or the FOLDER_NAME/bin to your PATH environment variable.

![Environment Variables](/img/environment-variables.png "Environment Variables")

Open terminal and cd into the bin location of cli folder.

![Terminal](/img/terminal-bin.png "Terminal")

Check spring version.

```
spring --version 
```
![Spring Version](/img/spring-version.png "Spring Version")

Create a groovy file.

![Spring Version](/img/app-groovy.png "Groovy")

![Spring Version](/img/groovy2.png "Groovy")


Create a controller in the groovy file.


![Controller](/img/groovy-controller.png "Controller")

Save by clicking `ctrl` + X and `ctrl` + Y.

Check if app.groovy is saved.

![Git command](/img/ls.png "ls command")

Execute this file

By using the following command.

```
./spring run app.groovy  
```

By running this command, you will be able to create a complete spring boot application.

![Spring Boot](/img/spring-application.png "Spring Application")

This application is running on the port 8080. So, we can invoke it on any browser by using the following url.

```
http://localhost:8080/
```

![Spring Boot](/img/localhost8080.png "Spring Application")






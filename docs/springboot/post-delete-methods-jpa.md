---
sidebar_position: 20
---

# Updating POST and DELETE methods on User Resource

We will convert deleteUser() method and createUser() method to use JPA. Let's make the changes in the UserJPAResource.java.

**Step 1:** Change the service of the deleteUser() method.

**Step 2:** Delete the return type because userRepository's delete() method does not return anything.

```java
@DeleteMapping("/jpa/users/{id}")  
public void deleteUser(@PathVariable int id)  
{  
  userRepository.deleteById(id);  
}  
```

If it fails, it throws an exception.

**Step 3:** Open the Postman and send a DELETE request with the URL http://localhost:8080/jpa/users/1.

![JPA](/img/postman16.png "DELETE request")

Status: 200 OK denotes that record has been deleted successfully.

Again send a DELETE request with the same URL http://localhost:8080/jpa/users/1. It returns a message "no entity with id 1 exist".

```java
{  
"timesatmp": "2017-07-20T12:10:29.988+0000",  
"message": "No class com.javatpoint.rest.webServices.restfulwebservices.user. User entity with id 1 exists! ",  
"details": "uri=/jpa/user/1"  
}  
```

Now we will generate a POST request to create a user.

**Step 4:** Send a POST request with the URL http://localhost:8080/jpa/users.

**Step 5:** Click on the Headers tab and make sure that the Content-Type is application/json.

![JPA](/img/postman17.png "DELETE request")

**Step 6:** Go to the Body tab and enter the name and dob of the user. We have entered the name James.

![JPA](/img/postman18.png "DELETE request")

**Step 7:** Click on the Send button. When we try to create a user, it throws ConstaintViolationException.

It is because hibernate uses a sequence. In the User entity, Id is generated value, so it creates a sequence for us. Hibernate is trying to insert a row with id 1. It conflicts with the data that we already have. To remove this conflict, do the following:

**Step 8:** Open the data.sql file and change the Ids.

```java
insert into user values(101, sysdate(), "John");  
insert into user values(102, sysdate(), "Robert");  
insert into user values(104, sysdate(), "Andrew");  
insert into user values(105, sysdate(), "Jack");  
```

**Step 9:** Again send a POST request. It returns the Status: 201 Created. The status shows that the user is created successfully.

![JPA](/img/postman19.png "POST request")


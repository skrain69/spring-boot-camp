---
sidebar_position: 21
---

# Implementing POST Service to Create a Post for a User

We will enable post-operation to create a post for the specific user.

**Step 1:** Open the UserJPAResource.java file and create a PostMapping to create a post.

```java
@PostMapping("/jpa/users/{id}/posts")  
public ResponseEntity<Object> createUser(@PathVariable int id, @RequestBody Post post)      
{  
  Optional<User> userOptional= userRepository.findById(id);  
  if(!userOptional.isPresent())  
  {  
    throw new UserNotFoundException("id: "+ id);  
  }  
  User user=userOptional.get();     
  //map the user to the post  
  post.setUser(user);  
  //save post to the database  
  postRepository.save(post);  
  //getting the path of the post and append id of the post to the URI   
  URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(post.getId()).toUri();  
  //returns the location of the created post  
  return ResponseEntity.created(location).build();  
}  
```

Step 2: Create a post repository.

### PostRepository.java

```java
package com.javatpoint.server.main.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

}  
```

**Step 3:** Open the Postman and send a POST request with the URI http://localhost:8080/jpa/users/102/posts. Under the Body tab, insert the post description.

![JPA](/img/postman20.png "POST request")

It returns the ***Status: 201 Created***. We can also see this post in the database by executing the query ***select *from post***;

![JPA](/img/postman21.png "Status: 201")
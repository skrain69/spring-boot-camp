---
sidebar_position: 2
---

# IntelliJ IDEA 👨‍💻 

IntelliJ IDEA is an intelligent, context-aware IDE for working with Java and other JVM languages like Kotlin, Scala, and Groovy on all sorts of applications. Additionally, IntelliJ IDEA Ultimate can help you develop full-stack web applications, thanks to its powerful integrated tools, support for JavaScript and related technologies, and advanced support for popular frameworks like Spring, Spring Boot, Jakarta EE, Micronaut, Quarkus, Helidon. Moreover, you can extend IntelliJ IDEA with free plugins developed by JetBrains, allowing you to work with other programming languages, including Go, Python, SQL, Ruby, and PHP.

## Getting Started

### Running IntelliJ IDEA for the first time
<iframe width="950" height="600" src="https://www.youtube.com/embed/c0efB_CKOYo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Creating your first Java application with IntelliJ IDEA
<iframe width="950" height="600" src="https://www.youtube.com/embed/H_XxH66lm3U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### IntelliJ IDEA keyboard shortcuts
<iframe width="950" height="600" src="https://www.youtube.com/embed/QYO5_riePOQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Docker in IntelliJ IDEA
<iframe width="950" height="600" src="https://www.youtube.com/embed/ck6xQqSOlpw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### IntelliJ IDEA ways to simplify your code
<iframe width="950" height="600" src="https://www.youtube.com/embed/HgWU25YwDfc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [IntelliJ IDEA](https://www.jetbrains.com/idea/features/)
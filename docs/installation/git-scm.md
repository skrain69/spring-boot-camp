---
sidebar_position: 1.1
---

# Git SCM 🌿

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.
![git](/img/git-branches.png "git branches")

## Getting Started

### Download and Installation
<iframe width="950" height="600" src="https://www.youtube.com/embed/E6-YSidPCu0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### What is Version Control
<iframe width="950" height="600" src="https://www.youtube.com/embed/8oRjP8yj2Wo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### What is Git
<iframe width="950" height="600" src="https://www.youtube.com/embed/uhtzxPU7Bz0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Get going with Git
<iframe width="950" height="600" src="https://www.youtube.com/embed/wmnSyrRBKTw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [git](https://git-scm.com/)
---
sidebar_position: 3
---

# Docker 🐋

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

## Getting Started

<iframe width="950" height="600" src="https://www.youtube.com/embed/S7NVloq0EBc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Virtualization

Your machine must have the following features for Docker Desktop to function correctly.

**WSL 2 and Windows Home**
1. Virtual Machine Platform
2. [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install)
3. Virtualization enabled in the BIOS
4. Hypervisor enabled at Windows startup
![WSL-VMP](/img/windows-features.png "WSL-VMP")

**Hyper-V**
On Windows 10 Pro or Enterprise, you can also use Hyper-V with the following features enabled:

1. [Hyper-V](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/hyper-v-technology-overview) installed and working
2. Virtualization enabled in the BIOS
3. Hypervisor enabled at Windows startup
![Hyper-V](/img/hyper-v.png "Hyper-V")

Docker Desktop requires Hyper-V as well as the Hyper-V Module for Windows Powershell to be installed and enabled. The Docker Desktop installer enables it for you.

Docker Desktop also needs two CPU hardware features to use Hyper-V: Virtualization and Second Level Address Translation (SLAT), which is also called Rapid Virtualization Indexing (RVI). On some systems, Virtualization must be enabled in the BIOS. The steps required are vendor-specific, but typically the BIOS option is called `Virtualization Technology (VTx)` or something similar. Run the command `systeminfo` to check all required Hyper-V features. See [Pre-requisites for Hyper-V on Windows 10](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/reference/hyper-v-requirements) for more details.

To install Hyper-V manually, see [Install Hyper-V on Windows 10](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v). A reboot is required after installation. If you install Hyper-V without rebooting, Docker Desktop does not work correctly.

From the start menu, type Turn Windows features on or off and press enter. In the subsequent screen, verify that Hyper-V is enabled.

:::info Note 
Virtualization must be enabled
:::

In addition to [Hyper-V](https://docs.docker.com/desktop/windows/troubleshoot/#hyper-v) or [WSL 2](https://docs.docker.com/desktop/windows/wsl/), virtualization must be enabled. Check the Performance tab on the Task Manager:

![virtualization](/img/virtualization.png "virtualization")

If you manually uninstall Hyper-V, WSL 2 or disable virtualization, Docker Desktop cannot start. See [Unable to run Docker for Windows on Windows 10 Enterprise](https://github.com/docker/for-win/issues/74).

**Hypervisor enabled at Windows startup**
If you have completed the steps described above and are still experiencing Docker Desktop startup issues, this could be because the Hypervisor is installed, but not launched during Windows startup. Some tools (such as older versions of Virtual Box) and video game installers disable hypervisor on boot. To reenable it:

Open an administrative console prompt.
Run `bcdedit /set hypervisorlaunchtype auto`.
Restart Windows.
You can also refer to the [Microsoft TechNet article](https://social.technet.microsoft.com/Forums/en-US/ee5b1d6b-09e2-49f3-a52c-820aafc316f9/hyperv-doesnt-work-after-upgrade-to-windows-10-1809?forum=win10itprovirt) on Code flow guard (CFG) settings.

## Additional Learning Materials
🔗 [Docker](https://docs.docker.com/get-started/)